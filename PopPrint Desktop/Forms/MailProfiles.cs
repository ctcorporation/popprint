﻿using System;
using System.Data;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using System.Xml.Linq;

namespace PopPrint_Desktop.Forms
{
    public partial class MailProfiles : Form
    {

        string ruleMode;
        int ruleRow;
        int _ruleId;
        string mailboxMode;
        int mailboxRow;
        private XElement ruleElement = new XElement("Rule",
                                        new XElement("RuleId"),
                                        new XElement("Name"),
                                        new XElement("IsActive", "false"),
                                        new XElement("PopAction"),
                                        new XElement("PopDelete", "false"),
                                        new XElement("ArchiveFile", "false"),
                                        new XElement("ArchiveLocation"),
                                        new XElement("AddressFrom"),
                                        new XElement("Subject"),
                                        new XElement("PrintQueue"),
                                        new XElement("FtpServer"),
                                        new XElement("FtpPort"),
                                        new XElement("FtpUserName"),
                                        new XElement("FtpPassword"),
                                        new XElement("FtpSecure"),
                                        new XElement("FtpFolder"),
                                        new XElement("FtpKeyLocation"),
                                        new XElement("FtpFingerPrint"),
                                        new XElement("SaveLocation"),
                                        new XElement("SaveDomain"),
                                        new XElement("SaveUser"),
                                        new XElement("SavePassword")


                                    );

        private DataSet dsMailbox;
        public MailProfiles()
        {
            InitializeComponent();
        }

        private void MailProfiles_Load(object sender, EventArgs e)
        {
            dsMailbox = new DataSet();
            ClearErrorImages();
            btnNew.Enabled = true;
            btnSave.Enabled = false;
            btnCancel.Enabled = false;
            GetPrinterList();
            GetProfileXMLData();
            rbAll.Checked = true;
            gbFtpDetails.Visible = false;
            gbPrinterDetails.Visible = false;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #region Methods

        public void FilterActiveProfiles()
        {
            foreach (Control control in gbRules.Controls)
            {
                if (control is RadioButton)
                {

                    if (((RadioButton)control).Checked)
                    {
                        switch (((RadioButton)control).Name)
                        {
                            case "rbAll":
                                {
                                    break;
                                }
                            case "rbActive":
                                {
                                    break;
                                }
                            case "rbInactive":
                                {
                                    break;
                                }
                        }
                    }
                }

            }
            //    DataView dv = new DataView(dsMailbox.Tables["Rule"]);
            //   dv.RowFilter = filter;
            // dgvRulesList.DataSource = dv;

        }
        public void GetPrinterList()
        {
            String myInstalledPrinters;
            for (int p = 0; p < PrinterSettings.InstalledPrinters.Count; p++)
            {
                myInstalledPrinters = PrinterSettings.InstalledPrinters[p];
                cmbPrintList.Items.Add(myInstalledPrinters);

            }
        }

        public void GetProfileXMLData()
        {

            if (!File.Exists(Globals.ProfileXml))
            {
                CreateProfileXML(Globals.ProfileXml);
            }

            dsMailbox.ReadXml(Globals.ProfileXml);
            dgvMailBoxList.DataSource = dsMailbox.Tables["Mailbox"];
            try
            {
                dgvRulesList.DataSource = dsMailbox.Tables["Mailbox"];
                dgvRulesList.DataMember = "Mailbox_Rule";
            }
            catch (ArgumentException ex)
            {
                CreateBlankRule();
                dgvMailBoxList.DataSource = null;
                dgvMailBoxList.Rows.Clear();
                GetProfileXMLData();

            }
            dgvMailBoxList.Refresh();
        }

        private void CreateBlankRule()
        {
            XDocument profiles = XDocument.Load(Globals.ProfileXml);

            var parent = (profiles.Descendants("Mailboxes").Elements("Mailbox").Where(x => x.Element("Rule") == null)).ToList();
            // var parent = (from e in profiles.Descendants("Mailbox")
            //               select e).ToList();
            foreach (var mb in parent)
            {
                mb.Add(ruleElement);
            }
            profiles.Save(Globals.ProfileXml);

        }

        public void CreateProfileXML(string appprofilexml)
        {
            //First create the XDocument instance
            // Note that the Xdocument is created in one line. I have separated it to make it easier to read
            XDocument xmlProfileConf = new XDocument(
                new XDeclaration("1.0", "UTF-8", "Yes"),
                new XElement("Mailboxes",
                    new XElement("Mailbox",
                        new XElement("PopServer"),
                        new XElement("PopUserName"),
                        new XElement("PopPassword"),
                        new XElement("PopPort"),
                        new XElement("PopSSL"),
                        new XElement("AddressTo"),
                        ruleElement
                                     )));
            xmlProfileConf.Save(appprofilexml);
        }
        void ColumnExists(string columnName, DataTable table, Type dataType)
        {
            DataColumnCollection dcColl = table.Columns;
            if (!dcColl.Contains(columnName))
            {
                DataColumn newColumn = new DataColumn
                {
                    ColumnName = columnName,
                    DataType = dataType
                };
                table.Columns.Add(newColumn);
            }
        }

        private void DoEditRule()
        {

            //DataRow row = dsMailbox.Tables["Rule"].Select("Rule_id = " + ruleRow);
            DataRow row = ((DataRowView)dgvRulesList.Rows[dgvRulesList.SelectedRows[0].Index].DataBoundItem).Row;
            row["Name"] = txtName.Text.Trim();
            row["IsActive"] = cbIsActive.Checked ? "true" : "false";
            switch (cmbProfileAction.Text.Trim())
            {
                case "Print":

                    break;
            }
            row["Mailbox_Id"] = ruleRow;
            row["AddressFrom"] = txtFromAddress.Text.Trim();
            row["Subject"] = txtSubject.Text.Trim();
            row["PopAction"] = cmbProfileAction.Text.Trim();

            row["PopDelete"] = radDeleteMailYes.Checked ? "true" : "false";

            row["ArchiveFile"] = radArchiveYes.Checked ? "true" : "false";

            row["ArchiveLocation"] = txtArchiveLocation.Text.Trim();
            btnCancelRule.Enabled = false;
            if (gbFtpDetails.Visible)
            {
                row["FtpServer"] = edFtpServer.Text;
                row["FtpPort"] = edFtpPort.Text;
                row["FtpUserName"] = edFtpUsername.Text;
                row["FtpPassword"] = edFtpPassword.Text;
                row["FtpFolder"] = edFtpfolders.Text;
                row["FtpSecure"] = cbFtpSecure.Checked ? "true" : "false";
                row["FtpFingerPrint"] = edFingerPrint.Text;
                row["FtpSSLCertificate"] = edKeyPath.Text;
            }
            if (gbPrinterDetails.Visible)
            {
                row["PrintQueue"] = cmbPrintList.Text.Trim();
            }

            if (gbSaveDetails.Visible)
            {
                ColumnExists("SaveLocation", dsMailbox.Tables["Rule"], typeof(string));
                row["SaveLocation"] = txtSaveFileLocation.Text;
                ColumnExists("SaveDomain", dsMailbox.Tables["Rule"], typeof(string));
                row["SaveDomain"] = txtSaveDomain.Text;
                ColumnExists("SaveUser", dsMailbox.Tables["Rule"], typeof(string));
                row["SaveUser"] = txtSaveUsername.Text;
                ColumnExists("SavePassword", dsMailbox.Tables["Rule"], typeof(string));
                row["SavePassword"] = txtSavePassword.Text;
            }
            DataTable tb = dsMailbox.Tables["Rule"];
            // dsMailbox.Tables["Rule"].Rows.Add(row);
            //Save changes to the table. 
            dsMailbox.AcceptChanges();
            //Write the Changes to the XML File immediately
            dsMailbox.WriteXml(Path.Combine(Globals.AppProfilePath, "PopPrintProfiles.xml"));
            btnSaveRule.Enabled = false;
            btnNewRule.Enabled = true;
        }
        private void DoNewRule()
        {
            MailboxesMailboxRule mbRule = new MailboxesMailboxRule();

            DataRow newRow = dsMailbox.Tables["Rule"].NewRow();



            newRow["RuleId"] = _ruleId;
            //Next I fill the fields of the newrecord instance
            newRow["Name"] = txtName.Text.Trim();
            newRow["IsActive"] = cbIsActive.Checked;

            switch (cmbProfileAction.Text.Trim())
            {
                case "Print":

                    break;
            }
            newRow["AddressFrom"] = txtFromAddress.Text.Trim();
            newRow["Subject"] = txtSubject.Text.Trim();
            newRow["PopAction"] = cmbProfileAction.Text.Trim();
            newRow["PopDelete"] = radDeleteMailYes.Checked ? "true" : "false";
            newRow["ArchiveFile"] = radArchiveYes.Checked ? "true" : "false";
            newRow["ArchiveLocation"] = txtArchiveLocation.Text.Trim();
            btnCancelRule.Enabled = false;
            if (gbFtpDetails.Visible)
            {
                newRow["FtpServer"] = edFtpServer.Text;
                newRow["FtpPort"] = edFtpPort.Text;
                newRow["FtpUserName"] = edFtpUsername.Text;
                newRow["FtpPassword"] = edFtpPassword.Text;
                newRow["FtpFolder"] = edFtpfolders.Text;
                newRow["FtpSecure"] = cbFtpSecure.Checked ? "true" : "false";
                if (cbFtpSecure.Checked)
                {
                    newRow["FtpFingerPrint"] = edFingerPrint.Text;
                }

            }
            if (gbPrinterDetails.Visible)
            {
                newRow["PrintQueue"] = cmbPrintList.Text.Trim();
            }
            if (gbSaveDetails.Visible)
            {
                ColumnExists("SaveLocation", dsMailbox.Tables["Rule"], typeof(string));
                newRow["SaveLocation"] = txtSaveFileLocation.Text;
                ColumnExists("SaveDomain", dsMailbox.Tables["Rule"], typeof(string));
                newRow["SaveDomain"] = txtSaveDomain.Text;
                ColumnExists("SaveUser", dsMailbox.Tables["Rule"], typeof(string));
                newRow["SaveUser"] = txtSaveUsername.Text;
                ColumnExists("SavePassword", dsMailbox.Tables["Rule"], typeof(string));
                newRow["SavePassword"] = txtSavePassword.Text;
            }
            DataTable tb = dsMailbox.Tables["Rule"];
            dsMailbox.Tables["Rule"].Rows.Add(newRow);
            //Save changes to the table. 
            dsMailbox.AcceptChanges();
            //Write the Changes to the XML File immediately
            dsMailbox.WriteXml(Path.Combine(Globals.AppProfilePath, "PopPrintProfiles.xml"));
            btnSaveRule.Enabled = false;
            btnNewRule.Enabled = true;
        }

        private int GetMailBoxId()
        {
            int temp = -1;
            var MaxID2 = dgvRulesList.Rows.Cast<DataGridViewRow>()
                        .Max(r => int.TryParse(r.Cells["RuleId"].Value.ToString(), out temp) ?
                                   temp : 1);
            return temp > 0 ? temp : 1;
        }

        private void DoNewMailbox()
        {
            try
            {

                // Get the Row ID of the Selected Row. 
                //If it is a new row then the Selected Row will be -1

                DataRow newRow;
                newRow = dsMailbox.Tables["Mailbox"].NewRow();
                newRow["PopServer"] = txtMailServer.Text.Trim();
                newRow["PopPort"] = txtMailPort.Text.Trim();
                newRow["PopUserName"] = txtMailUser.Text.Trim();
                newRow["PopPassword"] = txtMailPass.Text.Trim();
                newRow["PopSSL"] = cmbMailSecurity.Text.Trim();
                newRow["AddressTo"] = txtMailTo.Text;
                dsMailbox.Tables["Mailbox"].Rows.Add(newRow);
            }
            catch (Exception)
            {
                CreateProfileXML(Globals.ProfileXml);
                GetProfileXMLData();
                DoNewMailbox();
            }
            dsMailbox.AcceptChanges();
            //Write the Changes to the XML File immediately
            dsMailbox.WriteXml(Path.Combine(Globals.AppProfilePath, "PopPrintProfiles.xml"));

        }

        private void DoEditMailbox()
        {
            DataRow row = ((DataRowView)dgvMailBoxList.Rows[mailboxRow].DataBoundItem).Row;
            row["PopServer"] = txtMailServer.Text.Trim();
            row["PopPort"] = txtMailPort.Text.Trim();
            row["PopUserName"] = txtMailUser.Text.Trim();
            row["PopPassword"] = txtMailPass.Text.Trim();
            row["PopSSL"] = cmbMailSecurity.Text.Trim();
            row["AddressTo"] = txtMailTo.Text;
            // Get the Row ID of the Selected Row. 
            //If it is a new row then the Selected Row will be -1

            int mbId = (int)dgvMailBoxList.Rows[dgvMailBoxList.CurrentRow.Index].Cells["Mailbox_id"].Value;
            dsMailbox.AcceptChanges();
            //Write the Changes to the XML File immediately
            dsMailbox.WriteXml(Path.Combine(Globals.AppProfilePath, "PopPrintProfiles.xml"));
        }
        #endregion

        #region Helpers
        public void ClearErrorImages()
        {
            //This is just used to setup the form. hide error images etc. 
            foreach (Control control in this.Controls)
            {
                if (control is PictureBox)
                {
                    ((PictureBox)control).Visible = false;
                }
            }
        }

        public void ClearForm(Control controlCollection)
        {
            //This will recursively clear each TextBox and Set the ComboBox to the none selected option
            // Each control (ie the Form) is a container of Controls.
            // The Foreach loop checks each control if it is a Text Box or a Combobox and sets it. 
            // If the Control is another Container (ie Panel or Group Box) it will run the method again. 
            // These are called recursive loops. 
            foreach (Control control in controlCollection.Controls)
            {
                if (control is TextBox)
                {
                    ((TextBox)control).Text = string.Empty;
                }
                if (control is ComboBox)
                {
                    ((ComboBox)control).SelectedIndex = -1;
                }
                if (control is GroupBox)
                {
                    ClearForm(control);
                }

            }
        }


        #endregion

        private void dgvMailBoxList_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dgvMailBoxList_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private DataGridViewCellEventArgs mouseLocation;
        private void dgvMailBoxList_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            //This is the update/Edit
            //First get all the information to edit/View.
            mailboxMode = "Edit";
            mailboxRow = e.RowIndex;
            DataRow row = ((DataRowView)dgvMailBoxList.Rows[e.RowIndex].DataBoundItem).Row;
            txtMailServer.Text = row["PopServer"].ToString();
            txtMailPort.Text = row["PopPort"].ToString();
            txtMailUser.Text = row["PopUserName"].ToString();
            txtMailPass.Text = row["PopPassword"].ToString();
            cmbMailSecurity.Text = row["PopSSL"].ToString();
            txtMailTo.Text = row["AddressTo"].ToString();

            btnNew.Enabled = false;
            btnCancel.Enabled = true;
            btnSave.Enabled = true;
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            ClearForm(gbMailboxes);
            rbAll.Checked = true;
            if (!btnSave.Enabled)
            {
                btnSave.Enabled = true;
            }
            dgvMailBoxList.ClearSelection();
            btnNew.Enabled = false;
            btnCancel.Enabled = true;
            cmbMailSecurity.SelectedIndex = 0;
            mailboxMode = "New";
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (ValidateAll(gbMailboxes))
            {
                //Once Validated we can then save the record. 

                //First create a new instance of the Table.Row
                //This inherits all of the fields that the table has
                switch (mailboxMode)
                {
                    case "New":
                        DoNewMailbox();
                        break;
                    case "Edit":
                        DoEditMailbox();
                        break;
                }

                btnNew.Enabled = true;
                btnCancel.Enabled = false;
                //Save changes to the table. 
                dsMailbox.AcceptChanges();
                //Write the Changes to the XML File immediately
                dsMailbox.WriteXml(Path.Combine(Globals.AppProfilePath, "PopPrintProfiles.xml"));
                CreateBlankRule();
                btnSave.Enabled = false;
                MessageBox.Show("Job Mailbox Updated", "Save Job Profile");
                //GetProfileXMLData();
            }
        }

        public bool ValidateAll(Control controlCollection)
        {
            //This is the Validation rule so that the right information is saved.
            //_errorlist will hold a list of all errors/validations 

            string _errorList = "";
            bool result = true;
            //Reset the Error Icons.
            ClearErrorImages();

            if (controlCollection.Name == "gbRules")
            {

            }
            else if (controlCollection.Name == "gbMailboxes")
            {
                if (string.IsNullOrEmpty(txtMailServer.Text.Trim()))
                {
                    result = false;
                    _errorList += "Mail Server/IP cannot be blank. \r\n";
                    pbMailServer.Visible = true;
                }
                if (string.IsNullOrEmpty(txtMailPort.Text.Trim()))
                {
                    result = false;
                    _errorList += "Mail Server Port cannot be blank. \r\n";
                    pbMailServerPort.Visible = true;
                }
                if (string.IsNullOrEmpty(txtMailUser.Text.Trim()))
                {
                    result = false;
                    _errorList += "Username cannot be blank. \r\n";
                    pbMailUsername.Visible = true;
                }
                if (string.IsNullOrEmpty(txtMailPass.Text.Trim()))
                {
                    result = false;
                    _errorList += "Password cannot be blank. \r\n";
                    pbMailPassword.Visible = true;
                }
            }
            if (!result)
            {
                MessageBox.Show(_errorList, "Unable to Save Profile", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

            }
            return result;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("Are you sure you wish to cancel changes?", "Save Mailbox Profile", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (dr == DialogResult.Yes)
            {
                btnNew.Enabled = true;
                dgvMailBoxList.Rows[0].Selected = true;
                btnCancel.Enabled = false;
                btnSave.Enabled = false;
                mailboxMode = "Cancel";
                ClearForm(gbMailboxes);
            }
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            DataSet ds = new DataSet();
            ds.ReadXml(Path.Combine(Globals.SettingsPath, "PopPrintProfiles.xml"));
            dsMailbox.ReadXml(Path.Combine(Globals.SettingsPath, "PopPrintProfiles.xml"));
            dgvMailBoxList.DataSource = dsMailbox.Tables["Mailbox"];
            dgvMailBoxList.DataSource = ds.Tables[0];
        }

        private void btnTestMailSettings_Click(object sender, EventArgs e)
        {
            //I created an overloaded Constructor. (Look at the frmTestEmail)
            // In the Constructor I pass the mail settings to test. 
            TestEmail testEmail = new TestEmail(txtMailServer.Text, txtMailPort.Text, txtMailUser.Text, txtMailPass.Text, cmbMailSecurity.Text);
            testEmail.Show();
        }

        private void rbAll_CheckedChanged(object sender, EventArgs e)
        {
            FilterActiveProfiles();
        }

        private void dgvRulesList_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {

            DataRow row = ((DataRowView)dgvRulesList.Rows[e.RowIndex].DataBoundItem).Row;
            int i = 0;
            ruleRow = int.TryParse(row["Mailbox_Id"].ToString(), out i) ? i : 1;
            txtName.Text = row["Name"].ToString();
            txtFromAddress.Text = row["AddressFrom"].ToString();
            txtSubject.Text = row["subject"].ToString();
            bool stringToBool;
            if (bool.TryParse(row["IsActive"].ToString(), out stringToBool))
            {
                cbIsActive.Checked = bool.Parse(row["IsActive"].ToString());
            }
            else
            {
                if (row["IsActive"].ToString() == "Yes")
                {
                    cbIsActive.Checked = true;
                }
                else
                {
                    cbIsActive.Checked = false;
                }
            }

            if (bool.TryParse(row["ArchiveFile"].ToString(), out stringToBool))
            {
                if (bool.Parse(row["ArchiveFile"].ToString()))
                {
                    radArchiveYes.Checked = true;
                    txtArchiveLocation.Text = row["ArchiveLocation"].ToString();
                }
                else
                {
                    radArchiveNo.Checked = true;
                }
            }
            else
            {
                if (row["ArchiveFile"].ToString() == "Yes")
                {
                    radArchiveYes.Checked = true;
                    txtArchiveLocation.Text = row["ArchiveLocation"].ToString();
                }
                else
                {
                    radArchiveNo.Checked = true;
                }
            }
            switch (row["PopAction"])
            {
                case "FTP":
                    edFtpServer.Text = row["FtpServer"].ToString();
                    edFtpPort.Text = row["FtpPort"].ToString();
                    edFtpUsername.Text = row["FtpUserName"].ToString();
                    edFtpPassword.Text = row["FtpPassword"].ToString();
                    edFtpfolders.Text = row["FtpFolder"].ToString();
                    bool bOut = false;
                    if (bool.TryParse(row["FtpSecure"].ToString(), out bOut))
                    {
                        if (bOut)
                        {
                            cbFtpSecure.Checked = true;
                            edFingerPrint.Text = row["FtpFingerprint"].ToString();
                        }
                        else
                        {
                            cbFtpSecure.Checked = false;
                        }
                    }
                    break;
                case "Print":
                    cmbPrintList.Text = row["PrintQueue"].ToString();
                    break;

            }
            cmbProfileAction.Text = row["PopAction"].ToString();

            if (bool.TryParse(row["PopDelete"].ToString(), out stringToBool))
            {
                if (bool.Parse(row["PopDelete"].ToString()))
                {
                    radDeleteMailYes.Checked = true;
                }
                else
                {
                    radMailDeleteNo.Checked = true;
                }
            }
            else
            {
                if (row["PopDelete"].ToString() == "Yes")
                {
                    radDeleteMailYes.Checked = true;
                }
                else
                {
                    radMailDeleteNo.Checked = true;
                }
            }

            btnNewRule.Enabled = false;
            btnCancelRule.Enabled = true;
            btnSaveRule.Enabled = true;
            ruleMode = "Edit";
        }

        private void dgvRulesList_CellMouseEnter(object sender, DataGridViewCellEventArgs e)
        {
            mouseLocation = e;
        }

        private void btnNewRule_Click(object sender, EventArgs e)
        {
            ClearForm(gbRules);
            rbAll.Checked = true;
            if (!btnSaveRule.Enabled)
            {
                btnSaveRule.Enabled = true;
            }
            btnNewRule.Enabled = false;
            btnCancelRule.Enabled = true;
            ruleMode = "New";
            if (dgvRulesList.Rows.Count == 0)
            {
                CreateBlankRule();
            }
            _ruleId = GetMailBoxId();
        }

        private void btnSaveRule_Click(object sender, EventArgs e)
        {
            if (ValidateAll(gbRules))
            {
                //Once Validated we can then save the record. 
                btnNew.Enabled = true;
                btnCancel.Enabled = false;
                //First create a new instance of the Table.RowF
                //This inherits all of the fields that the table has

                switch (ruleMode)
                {
                    case "New":
                        DoNewRule();
                        break;
                    case "Cancel":
                        break;
                    case "Edit":
                        DoEditRule();
                        break;

                }
                MessageBox.Show("Mailbox Rule Updated", "Save Mailbox Rule");
            }
        }

        private void btnCancelRule_Click(object sender, EventArgs e)
        {
            {
                DialogResult dr = MessageBox.Show("Are you sure you wish to cancel changes?", "Save Mailbox Rules Profile", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (dr == DialogResult.Yes)
                {
                    btnNewRule.Enabled = true;
                    btnCancelRule.Enabled = false;
                    btnSaveRule.Enabled = false;
                    ClearForm(gbRules);
                    ruleMode = "Cancelled";
                }
            }
        }

        private void btnSaveFolderLoc_Click(object sender, EventArgs e)
        {

        }

        private void btnArchiveLocation_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fd = new FolderBrowserDialog();
            fd.SelectedPath = txtArchiveLocation.Text;
            fd.ShowDialog();
            txtArchiveLocation.Text = fd.SelectedPath;
        }

        private void cmbProfileAction_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (cmbProfileAction.Text)
            {
                case "Print":
                    gbPrinterDetails.Visible = true;
                    gbPrinterDetails.BringToFront();
                    gbFtpDetails.Visible = false;
                    gbSaveDetails.Visible = false;
                    break;
                case "FTP":
                    gbPrinterDetails.Visible = false;
                    gbFtpDetails.Visible = true;
                    gbFtpDetails.BringToFront();
                    gbSaveDetails.Visible = false;
                    break;
                case "Save":
                    gbPrinterDetails.Visible = false;
                    gbFtpDetails.Visible = false;
                    gbSaveDetails.Visible = true;
                    gbSaveDetails.BringToFront();
                    break;


            }
        }

        private void deleteProfileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (dgvMailBoxList.SelectedRows.Count > 0)
            {

                if (MessageBox.Show("Are you sure you wish to delete this profile", "Delete Message Profile", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    var mbId = dgvMailBoxList.SelectedRows[0];

                    var delrow = dgvRulesList.SelectedRows;
                    foreach (DataGridViewRow row in delrow)
                    {
                        string ruleID = row.Cells["ruleId"].Value.ToString();
                        RemoveRow(mbId.Cells["mbId"].Value.ToString(), ruleID);
                        GetProfileXMLData();
                    }

                }
            }

        }

        private void RemoveRow(string mailboxId, string ruleId)
        {
            var profile = XDocument.Load(Path.Combine(Globals.SettingsPath, "PopPrintProfiles.xml"));
            var mailbox = profile.Descendants("Mailbox")
                                .Where(x => x.Elements("MailBoxId").Any(a => a.Value == mailboxId));
            if (mailbox != null)
            {
                mailbox.Descendants("Rule").Where(y => y.Elements("Id").Any(a => a.Value == ruleId)).Remove();
            }
            profile.Save(Path.Combine(Globals.SettingsPath, "PopPrintProfiles.xml"));



        }
    }
}
