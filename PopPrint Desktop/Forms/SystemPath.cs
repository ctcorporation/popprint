﻿using System;
using System.Configuration;
using System.IO;
using System.Windows.Forms;

namespace PopPrint_Desktop.Forms
{
    public partial class SystemPath : Form
    {
        public SystemPath()
        {
            InitializeComponent();
        }

        private void btnPath_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fd = new FolderBrowserDialog();
            var dr = fd.ShowDialog();
            if (dr == DialogResult.OK)
            {
                if (!string.IsNullOrEmpty(fd.SelectedPath))
                {
                    if (!Directory.Exists(fd.SelectedPath))
                    {
                        bool created = false;
                        while (!created)
                        {
                            try
                            {
                                Directory.CreateDirectory(fd.SelectedPath);
                                created = true;

                            }
                            catch (UnauthorizedAccessException ex)
                            {
                                MessageBox.Show("Unable to create a folder here", ex.GetType().Name, MessageBoxButtons.OK, MessageBoxIcon.Error);
                                created = false;
                            }

                        }


                    }
                    edPath.Text = fd.SelectedPath;
                    ConfigurationManager.AppSettings.Set("DefaultAppPath", edPath.Text);
                }
            }
        }
    }
}
