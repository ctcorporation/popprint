﻿namespace PopPrint_Desktop.Forms
{
    partial class Settings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Settings));
            this.cbRunOnStart = new System.Windows.Forms.CheckBox();
            this.bbProfileLoc = new System.Windows.Forms.Button();
            this.txtTimer = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.gbArchiveFreq = new System.Windows.Forms.GroupBox();
            this.radDaily = new System.Windows.Forms.RadioButton();
            this.radMonthly = new System.Windows.Forms.RadioButton();
            this.radWeekly = new System.Windows.Forms.RadioButton();
            this.txtProfileLocation = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnLogLocation = new System.Windows.Forms.Button();
            this.txtLogLocation = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.bbHtmlTemplate = new System.Windows.Forms.Button();
            this.edHtmlTemplate = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.btnWinSCP = new System.Windows.Forms.Button();
            this.edWinSCP = new System.Windows.Forms.TextBox();
            this.btnAttLocation = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.txtAttLocation = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.bbCancel = new Syncfusion.WinForms.Controls.SfButton();
            this.btnSave = new Syncfusion.WinForms.Controls.SfButton();
            ((System.ComponentModel.ISupportInitialize)(this.txtTimer)).BeginInit();
            this.gbArchiveFreq.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // cbRunOnStart
            // 
            this.cbRunOnStart.AutoSize = true;
            this.cbRunOnStart.Location = new System.Drawing.Point(218, 188);
            this.cbRunOnStart.Name = "cbRunOnStart";
            this.cbRunOnStart.Size = new System.Drawing.Size(134, 17);
            this.cbRunOnStart.TabIndex = 40;
            this.cbRunOnStart.Text = "Run in Auto on Startup";
            this.cbRunOnStart.UseVisualStyleBackColor = true;
            // 
            // bbProfileLoc
            // 
            this.bbProfileLoc.Location = new System.Drawing.Point(421, 152);
            this.bbProfileLoc.Name = "bbProfileLoc";
            this.bbProfileLoc.Size = new System.Drawing.Size(25, 23);
            this.bbProfileLoc.TabIndex = 39;
            this.bbProfileLoc.Text = "...";
            this.bbProfileLoc.UseVisualStyleBackColor = true;
            this.bbProfileLoc.Click += new System.EventHandler(this.bbProfileLoc_Click);
            // 
            // txtTimer
            // 
            this.txtTimer.Location = new System.Drawing.Point(135, 187);
            this.txtTimer.Name = "txtTimer";
            this.txtTimer.Size = new System.Drawing.Size(46, 20);
            this.txtTimer.TabIndex = 38;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 187);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(84, 13);
            this.label4.TabIndex = 37;
            this.label4.Text = "Timer in Minutes";
            // 
            // gbArchiveFreq
            // 
            this.gbArchiveFreq.Controls.Add(this.radDaily);
            this.gbArchiveFreq.Controls.Add(this.radMonthly);
            this.gbArchiveFreq.Controls.Add(this.radWeekly);
            this.gbArchiveFreq.Location = new System.Drawing.Point(12, 84);
            this.gbArchiveFreq.Name = "gbArchiveFreq";
            this.gbArchiveFreq.Size = new System.Drawing.Size(202, 47);
            this.gbArchiveFreq.TabIndex = 36;
            this.gbArchiveFreq.TabStop = false;
            this.gbArchiveFreq.Text = "Archive Frequency";
            // 
            // radDaily
            // 
            this.radDaily.AutoSize = true;
            this.radDaily.Location = new System.Drawing.Point(146, 19);
            this.radDaily.Name = "radDaily";
            this.radDaily.Size = new System.Drawing.Size(48, 17);
            this.radDaily.TabIndex = 9;
            this.radDaily.TabStop = true;
            this.radDaily.Tag = "ArcFreq";
            this.radDaily.Text = "Daily";
            this.radDaily.UseVisualStyleBackColor = true;
            // 
            // radMonthly
            // 
            this.radMonthly.AutoSize = true;
            this.radMonthly.Location = new System.Drawing.Point(11, 19);
            this.radMonthly.Name = "radMonthly";
            this.radMonthly.Size = new System.Drawing.Size(62, 17);
            this.radMonthly.TabIndex = 7;
            this.radMonthly.TabStop = true;
            this.radMonthly.Tag = "ArcFreq";
            this.radMonthly.Text = "Monthly";
            this.radMonthly.UseVisualStyleBackColor = true;
            // 
            // radWeekly
            // 
            this.radWeekly.AutoSize = true;
            this.radWeekly.Location = new System.Drawing.Point(79, 19);
            this.radWeekly.Name = "radWeekly";
            this.radWeekly.Size = new System.Drawing.Size(61, 17);
            this.radWeekly.TabIndex = 8;
            this.radWeekly.TabStop = true;
            this.radWeekly.Tag = "ArcFreq";
            this.radWeekly.Text = "Weekly";
            this.radWeekly.UseVisualStyleBackColor = true;
            // 
            // txtProfileLocation
            // 
            this.txtProfileLocation.Location = new System.Drawing.Point(135, 154);
            this.txtProfileLocation.Name = "txtProfileLocation";
            this.txtProfileLocation.ReadOnly = true;
            this.txtProfileLocation.Size = new System.Drawing.Size(288, 20);
            this.txtProfileLocation.TabIndex = 35;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 157);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(117, 13);
            this.label6.TabIndex = 34;
            this.label6.Text = "Default Profile Location";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(12, 134);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(97, 13);
            this.label5.TabIndex = 33;
            this.label5.Text = "System Settings";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(12, 68);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(107, 13);
            this.label3.TabIndex = 32;
            this.label3.Text = "Archiving Options";
            // 
            // btnLogLocation
            // 
            this.btnLogLocation.Location = new System.Drawing.Point(421, 34);
            this.btnLogLocation.Name = "btnLogLocation";
            this.btnLogLocation.Size = new System.Drawing.Size(25, 20);
            this.btnLogLocation.TabIndex = 31;
            this.btnLogLocation.Text = "...";
            this.btnLogLocation.UseVisualStyleBackColor = true;
            this.btnLogLocation.Click += new System.EventHandler(this.btnLogLocation_Click);
            // 
            // txtLogLocation
            // 
            this.txtLogLocation.Location = new System.Drawing.Point(135, 34);
            this.txtLogLocation.Name = "txtLogLocation";
            this.txtLogLocation.Size = new System.Drawing.Size(288, 20);
            this.txtLogLocation.TabIndex = 30;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 37);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(89, 13);
            this.label2.TabIndex = 29;
            this.label2.Text = "Logging Location";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(99, 13);
            this.label1.TabIndex = 28;
            this.label1.Text = "Logging Options";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.bbHtmlTemplate);
            this.groupBox1.Controls.Add(this.edHtmlTemplate);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.btnWinSCP);
            this.groupBox1.Controls.Add(this.edWinSCP);
            this.groupBox1.Controls.Add(this.btnAttLocation);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.txtAttLocation);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Location = new System.Drawing.Point(13, 215);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(455, 165);
            this.groupBox1.TabIndex = 41;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "File Locations";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(118, 125);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(286, 13);
            this.label12.TabIndex = 35;
            this.label12.Text = "(Default HTML Template to be used for Printing email body)";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(6, 102);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(84, 13);
            this.label11.TabIndex = 32;
            this.label11.Text = "HTML Template";
            // 
            // bbHtmlTemplate
            // 
            this.bbHtmlTemplate.Location = new System.Drawing.Point(407, 102);
            this.bbHtmlTemplate.Name = "bbHtmlTemplate";
            this.bbHtmlTemplate.Size = new System.Drawing.Size(25, 20);
            this.bbHtmlTemplate.TabIndex = 34;
            this.bbHtmlTemplate.Text = "...";
            this.bbHtmlTemplate.UseVisualStyleBackColor = true;
            this.bbHtmlTemplate.Click += new System.EventHandler(this.bbHtmlTemplate_Click);
            // 
            // edHtmlTemplate
            // 
            this.edHtmlTemplate.Location = new System.Drawing.Point(121, 102);
            this.edHtmlTemplate.Name = "edHtmlTemplate";
            this.edHtmlTemplate.Size = new System.Drawing.Size(288, 20);
            this.edHtmlTemplate.TabIndex = 33;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(119, 84);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(129, 13);
            this.label10.TabIndex = 27;
            this.label10.Text = "(Location of WinSCP.exe)";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(7, 66);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(103, 13);
            this.label9.TabIndex = 24;
            this.label9.Text = "WinSCP Executable";
            // 
            // btnWinSCP
            // 
            this.btnWinSCP.Location = new System.Drawing.Point(408, 63);
            this.btnWinSCP.Name = "btnWinSCP";
            this.btnWinSCP.Size = new System.Drawing.Size(25, 20);
            this.btnWinSCP.TabIndex = 26;
            this.btnWinSCP.Text = "...";
            this.btnWinSCP.UseVisualStyleBackColor = true;
            this.btnWinSCP.Click += new System.EventHandler(this.btnWinSCP_Click);
            // 
            // edWinSCP
            // 
            this.edWinSCP.Location = new System.Drawing.Point(122, 63);
            this.edWinSCP.Name = "edWinSCP";
            this.edWinSCP.Size = new System.Drawing.Size(288, 20);
            this.edWinSCP.TabIndex = 25;
            // 
            // btnAttLocation
            // 
            this.btnAttLocation.Location = new System.Drawing.Point(408, 24);
            this.btnAttLocation.Name = "btnAttLocation";
            this.btnAttLocation.Size = new System.Drawing.Size(25, 20);
            this.btnAttLocation.TabIndex = 21;
            this.btnAttLocation.Text = "...";
            this.btnAttLocation.UseVisualStyleBackColor = true;
            this.btnAttLocation.Click += new System.EventHandler(this.btnAttLocation_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(7, 24);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(34, 13);
            this.label7.TabIndex = 18;
            this.label7.Text = "Temp";
            // 
            // txtAttLocation
            // 
            this.txtAttLocation.Location = new System.Drawing.Point(122, 24);
            this.txtAttLocation.Name = "txtAttLocation";
            this.txtAttLocation.Size = new System.Drawing.Size(288, 20);
            this.txtAttLocation.TabIndex = 20;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(119, 47);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(247, 13);
            this.label8.TabIndex = 19;
            this.label8.Text = "(Temporary location where Attachments are stored)";
            // 
            // bbCancel
            // 
            this.bbCancel.AccessibleName = "Button";
            this.bbCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bbCancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bbCancel.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.bbCancel.Font = new System.Drawing.Font("Segoe UI Semibold", 9F);
            this.bbCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bbCancel.Location = new System.Drawing.Point(408, 389);
            this.bbCancel.Name = "bbCancel";
            this.bbCancel.Size = new System.Drawing.Size(63, 28);
            this.bbCancel.Style.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bbCancel.Style.Image = global::PopPrint_Desktop.Properties.Resources.Close;
            this.bbCancel.TabIndex = 42;
            this.bbCancel.Text = "C&lose";
            this.bbCancel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bbCancel.UseVisualStyleBackColor = false;
            this.bbCancel.Click += new System.EventHandler(this.bbCancel_Click);
            // 
            // btnSave
            // 
            this.btnSave.AccessibleName = "Button";
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnSave.Font = new System.Drawing.Font("Segoe UI Semibold", 9F);
            this.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSave.Location = new System.Drawing.Point(340, 389);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(62, 28);
            this.btnSave.Style.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnSave.Style.Image = global::PopPrint_Desktop.Properties.Resources.Save;
            this.btnSave.TabIndex = 43;
            this.btnSave.Text = "&Save";
            this.btnSave.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // Settings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.bbCancel;
            this.ClientSize = new System.Drawing.Size(478, 429);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.bbCancel);
            this.Controls.Add(this.cbRunOnStart);
            this.Controls.Add(this.bbProfileLoc);
            this.Controls.Add(this.txtTimer);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.gbArchiveFreq);
            this.Controls.Add(this.txtProfileLocation);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnLogLocation);
            this.Controls.Add(this.txtLogLocation);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.groupBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Settings";
            this.Text = "Settings";
            this.Load += new System.EventHandler(this.Settings_Load);
            ((System.ComponentModel.ISupportInitialize)(this.txtTimer)).EndInit();
            this.gbArchiveFreq.ResumeLayout(false);
            this.gbArchiveFreq.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox cbRunOnStart;
        private System.Windows.Forms.Button bbProfileLoc;
        private System.Windows.Forms.NumericUpDown txtTimer;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox gbArchiveFreq;
        private System.Windows.Forms.RadioButton radDaily;
        private System.Windows.Forms.RadioButton radMonthly;
        private System.Windows.Forms.RadioButton radWeekly;
        private System.Windows.Forms.TextBox txtProfileLocation;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnLogLocation;
        private System.Windows.Forms.TextBox txtLogLocation;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button btnWinSCP;
        private System.Windows.Forms.TextBox edWinSCP;
        private System.Windows.Forms.Button btnAttLocation;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtAttLocation;
        private System.Windows.Forms.Label label8;
        private Syncfusion.WinForms.Controls.SfButton bbCancel;
        private Syncfusion.WinForms.Controls.SfButton btnSave;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button bbHtmlTemplate;
        private System.Windows.Forms.TextBox edHtmlTemplate;
    }
}