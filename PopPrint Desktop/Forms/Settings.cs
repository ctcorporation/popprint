﻿using PopPrint_Desktop.Classes;
using System;
using System.IO;
using System.Windows.Forms;
using System.Xml.Linq;

namespace PopPrint_Desktop.Forms
{
    public partial class Settings : Form
    {
        SystemSettings settings;


        public Settings()
        {
            InitializeComponent();
        }

        private void bbCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void CheckDir(string directory)
        {
            if (!Directory.Exists(txtLogLocation.Text))
            {
                Directory.CreateDirectory(txtLogLocation.Text);
            }

        }

        private void Settings_Load(object sender, EventArgs e)
        {
            switch (Globals.ArcFrequency)
            {
                case "Daily":
                    radDaily.Checked = true;
                    break;
                case "Weekly":
                    radWeekly.Checked = true;
                    break;
                case "Monthly":
                    radMonthly.Checked = true;
                    break;
            }
            txtLogLocation.Text = Globals.LogPath;
            txtProfileLocation.Text = Globals.AppProfilePath;
            txtTimer.Value = Globals.TimerInt;
            cbRunOnStart.Checked = Globals.RunOnStart;
            txtAttLocation.Text = Globals.AttachmentPath;
            edWinSCP.Text = Globals.WinSCPPath;
            edHtmlTemplate.Text = Globals.HtmlTemplate;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            string s = Path.Combine(Globals.SettingsPath, Globals.AppName + ".xml");
            settings = new SystemSettings(s);

            var xmlConfig = XDocument.Load(s);
            foreach (RadioButton rb in gbArchiveFreq.Controls)
            {
                switch (rb.Text)
                {
                    case "Daily":
                        if (radDaily.Checked)
                        {
                            Globals.ArcFrequency = "Daily";
                        }

                        break;
                    case "Weekly":
                        if (radWeekly.Checked)
                        {
                            Globals.ArcFrequency = "Weekly";
                        }

                        break;
                    case "Monthly":
                        if (radMonthly.Checked)
                        {
                            Globals.ArcFrequency = "Monthly";
                        }
                        break;
                }
            }
            CheckDir(txtLogLocation.Text);
            settings.SetElement("SystemSettings", "LogLocation", txtLogLocation.Text);
            settings.SetElement("SystemSettings", "ProfileLocation", txtProfileLocation.Text);
            CheckDir(txtProfileLocation.Text);
            settings.SetElement("SystemSettings", "AttachmentLocation", txtAttLocation.Text);
            CheckDir(txtAttLocation.Text);
            settings.SetElement("SystemSettings", "WinSCPLocation", edWinSCP.Text);
            settings.SetElement("SystemSettings", "HtmlTemplate", edHtmlTemplate.Text);
            settings.SetElement("SystemSettings", "RunOnStart", cbRunOnStart.Checked ? "true" : "false");
            settings.SetElement("SystemSettings", "TimerInterval", txtTimer.Value.ToString());


            foreach (RadioButton rb in gbArchiveFreq.Controls)
            {
                switch (rb.Text)
                {
                    case "Daily":
                        if (radDaily.Checked)
                        {
                            Globals.ArcFrequency = "Daily";
                        }

                        break;
                    case "Weekly":
                        if (radWeekly.Checked)
                        {
                            Globals.ArcFrequency = "Weekly";
                        }

                        break;
                    case "Monthly":
                        if (radMonthly.Checked)
                        {
                            Globals.ArcFrequency = "Monthly";
                        }

                        break;
                }
            }
            settings.SetElement("ArchiveSettings", "ArchiveFrequency", Globals.ArcFrequency);
            settings.UpdateSettings();

            Globals.AppProfilePath = txtProfileLocation.Text;
            Globals.LogPath = txtLogLocation.Text;
            Globals.WinSCPPath = edWinSCP.Text;
            Globals.TimerInt = (int)txtTimer.Value;
            Globals.TempPath = txtAttLocation.Text;
            MessageBox.Show("System Settings Saved");
        }

        private void btnLogLocation_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fd = new FolderBrowserDialog();
            fd.SelectedPath = string.IsNullOrEmpty(txtLogLocation.Text) ? Globals.SettingsPath : txtLogLocation.Text;
            var dr = fd.ShowDialog();
            if (dr == DialogResult.OK)
            {
                txtLogLocation.Text = fd.SelectedPath;
            }
        }

        private void bbProfileLoc_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fd = new FolderBrowserDialog();
            fd.SelectedPath = string.IsNullOrEmpty(txtProfileLocation.Text) ? Globals.SettingsPath : txtProfileLocation.Text;
            var dr = fd.ShowDialog();
            if (dr == DialogResult.OK)
            {
                txtProfileLocation.Text = fd.SelectedPath;
            }
        }

        private void btnAttLocation_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fd = new FolderBrowserDialog();
            fd.SelectedPath = string.IsNullOrEmpty(txtAttLocation.Text) ? Globals.SettingsPath : txtAttLocation.Text;
            var dr = fd.ShowDialog();
            if (dr == DialogResult.OK)
            {
                txtAttLocation.Text = fd.SelectedPath;
            }
        }

        private void btnWinSCP_Click(object sender, EventArgs e)
        {
            OpenFileDialog fd = new OpenFileDialog();
            var dr = fd.ShowDialog();
            if (dr == DialogResult.OK)
            {
                edWinSCP.Text = Path.GetFullPath(fd.FileName);
            }
        }

        private void bbHtmlTemplate_Click(object sender, EventArgs e)
        {
            OpenFileDialog fd = new OpenFileDialog();
            var dr = fd.ShowDialog();
            if (dr == DialogResult.OK)
            {
                edHtmlTemplate.Text = Path.GetFullPath(fd.FileName);
            }
        }
    }
}
