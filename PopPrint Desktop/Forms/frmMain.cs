﻿using PopPrint_Desktop.Classes;
using PopPrint_Desktop.Forms;
using System;
using System.Configuration;
using System.Data;
using System.IO;
using System.Windows.Forms;

namespace PopPrint_Desktop
{
    public partial class frmMain : Form
    {
        public frmMain()
        {
            InitializeComponent();
            Globals.SettingsPath = ConfigurationManager.AppSettings.Get("DefaultAppPath");
            Globals.AppName = ConfigurationManager.AppSettings.Get("AppName");
            Globals.LogFileName = ConfigurationManager.AppSettings.Get("LogFileName");

        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void settingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Settings settings = new Settings();
            var dr = settings.ShowDialog();
            if (dr == DialogResult.OK)
            {
                LoadSettings();
            }
        }

        private void LoadSettings()
        {
            string s = Path.Combine(Globals.SettingsPath, Globals.AppName + ".xml");
            SystemSettings systemSettings = new SystemSettings(s);
            bool setupNeeded = false;
            var profPath = systemSettings.GetElement("SystemSettings", "ProfileLocation");
            if (profPath == null)
            {
                setupNeeded = true;
            }
            else
            {
                Globals.AppProfilePath = profPath.Value;
                Globals.ProfileXml = Path.Combine(profPath.Value, "PopPrintProfiles.xml");
            }
            var logpath = systemSettings.GetElement("SystemSettings", "LogLocation");
            if (logpath == null)
                setupNeeded = true;
            else
                Globals.LogPath = logpath.Value;
            var winscp = systemSettings.GetElement("SystemSettings", "WinSCPLocation");
            if (winscp == null)
                setupNeeded = true;
            else
                Globals.WinSCPPath = winscp.Value;
            var htmlTemplate = systemSettings.GetElement("SystemSettings", "HtmlTemplate");
            if (htmlTemplate == null)
                setupNeeded = true;
            else
                Globals.HtmlTemplate = htmlTemplate.Value;
            var runOnstart = systemSettings.GetElement("SystemSettings", "RunOnStart");
            if (runOnstart == null)
                setupNeeded = true;
            else
                Globals.RunOnStart = bool.Parse(runOnstart.Value);
            var attLoc = systemSettings.GetElement("SystemSettings", "AttachmentLocation");
            if (attLoc == null)
                setupNeeded = true;
            else
                Globals.AttachmentPath = attLoc.Value;
            var timer = systemSettings.GetElement("SystemSettings", "TimerInterval");
            if (timer == null)
                setupNeeded = true;
            else
                Globals.TimerInt = int.Parse(timer.Value);
            var arcFreq = systemSettings.GetElement("ArchiveSettings", "ArchiveFrequency");
            if (arcFreq == null)
                setupNeeded = true;
            else
                Globals.ArcFrequency = arcFreq.Value;

            if (setupNeeded)
            {
                MessageBox.Show("Pop Print settings are missing. Setup needed.");

            }

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            LoadSettings();
            LoadLog();
        }

        private void LoadLog()
        {
            if (!string.IsNullOrEmpty(Globals.LogPath))
            {
                string log = Path.Combine(Globals.LogPath, Globals.LogFileName);
                if (File.Exists(log))
                {
                    DataSet dsLog = new DataSet();
                    dsLog.ReadXml(log);
                    grdErrorLog.Invoke(new Action(() => grdErrorLog.DataSource = dsLog));
                    grdErrorLog.Invoke(new Action(() => grdErrorLog.DataMember = dsLog.Tables[0].TableName));
                    grdErrorLog.Invoke(new Action(() => grdErrorLog.Sort(grdErrorLog.Columns[0], System.ComponentModel.ListSortDirection.Descending)));
                }
            }

        }

        private void btnReload_Click(object sender, EventArgs e)
        {
            LoadLog();
        }

        private void mailProfilesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MailProfiles mailProfiles = new MailProfiles();
            var dr = mailProfiles.ShowDialog();
            if (dr == DialogResult.OK)
            {

            }

        }
    }
}
