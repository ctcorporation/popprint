﻿using PopPrint_Desktop.Classes;
using System;
using System.Collections.Generic;

namespace PopPrint_Desktop.Interfaces
{
    public interface IMsgSummary
    {
        string MsgBcc { get; set; }
        string MsgBody { get; set; }
        string MsgCC { get; set; }
        List<MsgAtt> MsgFile { get; set; }
        string MsgFrom { get; set; }
        string MsgHtmlBody { get; set; }
        int MsgId { get; set; }
        string MsgStatus { get; set; }
        string MsgSubject { get; set; }
        string MsgTo { get; set; }
        DateTime MsgDate { get; set; }
    }
}