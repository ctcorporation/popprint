﻿using System;
using System.IO;
using System.Xml.Linq;

namespace PopPrint_Desktop.Classes
{
    public class PopPrintResources
    {
        public enum EventCode
        {
            E000, //Timer Event - Normal Operation

            E001, //Timer Stopped 

            E002, //Timer Started

            I100, //Pop3 - Succesful

            E101, //Pop3 - User login failed

            E102, //Pop3 - Server not found

            E103, //Pop3 - No Messages Found

            E104, //Pop3 - Timeout

            E105, //Pop3 - Unable to retrieve Message       

            I300, //FTP - Successful

            E201, //Profile - Invalid File Type - Message Discarded

            E202, // HTML -PDF Format Error

            E203, //Profile - Profile File Not Found

            E204, //Profile - Invalid format

            E205, //HTML - Default Template not found. 

            E301, //FTP - Server Not found 

            E302, //FTP - User credentials failed

            E303, //FTP - Read Folder Access failed

            E304, //FTP - Write Access failed

            E305, //FTP - Protocol Error

            E306, //FTP - Timeout Error.

            E307, //FTP - File Send Error.

            E308, //FTP - Connection Issue

            E309, //FTP - WINSCP Error. 

            I400, //Printer - Job Successful

            E401, //Printer - Not Found

            E402, //Printer - Security rights Error

            E403, //Printer - Timeout Error.

            E404, //Printer - File Print Error

            E405, //Printer - Print Queue Not Specified

            E406, //Printer - Un-Supported File Type



        }

        public static void WriteLog(string filename, PopPrintResources.EventCode eventcode, string process, string info, MailboxesMailboxRule mbrule, MsgSummary msg)
        {
            string logFile = Path.Combine(Globals.LogPath, "PopPrintLog.XML");
            XDocument log;
            if (!File.Exists(logFile))
            {
                log = new XDocument(
                    new XDeclaration("1.0", "UTF-8", "Yes"),
                    new XElement("PopPrintLog",
                        new XElement("Log",
                            new XElement("OpDate"),
                            new XElement("Process"),
                            new XElement("Code"),
                            new XElement("FileName"),
                            new XElement("MailboxRule"),
                            new XElement("Information"),
                            new XElement("From"),
                            new XElement("To"),
                            new XElement("Subject"),
                            new XElement("MsgDate"))));
                log.Save(@logFile);
            }
            log = new XDocument(XDocument.Load(logFile));
            var logEntry = new XElement("Log",
                            new XElement("OpDate", DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")),
                            new XElement("Process", process),
                            new XElement("Code", eventcode.ToString()),
                            new XElement("FileName", filename),
                            new XElement("MailboxRule", mbrule != null ? mbrule.Name : null),
                            new XElement("Information", info),
                            new XElement("From", msg != null ? msg.MsgFrom : null),
                            new XElement("To", msg != null ? msg.MsgTo : null),
                            new XElement("Subject", msg != null ? msg.MsgSubject : null),
                            new XElement("MsgDate", msg != null ? msg.MsgDate.ToString("dd/MM/yyyy HH:mm:ss") : null));
            log.Element("PopPrintLog").Add(logEntry);
            log.Save(@logFile);

        }






    }
}
