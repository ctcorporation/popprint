﻿namespace PopPrint_Desktop
{
    public static class Globals
    {
        public static string SettingsPath { get; set; }
        public static string AppName { get; set; }
        public static string LogFileName { get; set; }
        public static string AppProfilePath { get; set; }
        public static string ProfileXml { get; set; }
        public static string ArcFrequency { get; set; }
        public static string LogPath { get; set; }
        public static int TimerInt { get; set; }
        public static string TempPath { get; set; }
        public static string AttachmentPath { get; set; }
        public static bool RunOnStart { get; set; }
        public static string WinSCPPath { get; internal set; }
        public static string HtmlTemplate { get; set; }
    }
}
