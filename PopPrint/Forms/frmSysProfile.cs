﻿using System;
using System.IO;
using System.Windows.Forms;
using System.Xml.Linq;

namespace PopPrint
{
    public partial class frmSysProfile : Form
    {
        public frmSysProfile()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnLogLocation_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fd = new FolderBrowserDialog();
            fd.SelectedPath = txtLogLocation.Text;
            fd.ShowDialog();
            txtLogLocation.Text = fd.SelectedPath;
        }

        private void btnProfileLocation_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fd = new FolderBrowserDialog();
            fd.SelectedPath = txtProfileLocation.Text;
            fd.ShowDialog();
            txtProfileLocation.Text = fd.SelectedPath;
        }

        private void frmSysProfile_Load(object sender, EventArgs e)
        {
            switch (Globals.ArcFrequency)
            {
                case "Daily":
                    radDaily.Checked = true;
                    break;
                case "Weekly":
                    radWeekly.Checked = true;
                    break;
                case "Monthly":
                    radMonthly.Checked = true;
                    break;
            }
            txtLogLocation.Text = Globals.LogPath;
            txtProfileLocation.Text = Globals.AppProfilePath;
            txtTimer.Value = Globals.TimerInt;
            cbRunOnStart.Checked = Globals.RunOnStart;
            txtAttLocation.Text = Globals.TempPath;
            edWinSCP.Text = Globals.WinSCPPath;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            string s = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name + "-CONFIG.XML";
            string appPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "PopPrint");
            s = Path.Combine(appPath, s);
            if (!File.Exists(s))
            {
                XDocument xmlConf = new XDocument(
                        new XDeclaration("1.0", "UTF-8", "Yes"),
                        new XElement("PopPrint",
                        new XElement("LogSettings",
                            new XElement("LogLocation")),
                        new XElement("SystemSettings",
                            new XElement("ProfileLocation"),
                            new XElement("AttachmentLocation"),
                            new XElement("WinSCPLocation"),
                            new XElement("RunOnStart")),
                        new XElement("ArchiveSettings",
                            new XElement("ArchiveFrequency"))));
                xmlConf.Save(s);
            }

            foreach (RadioButton rb in gbArchiveFreq.Controls)
            {
                switch (rb.Text)
                {
                    case "Daily":
                        if (radDaily.Checked)
                        {
                            Globals.ArcFrequency = "Daily";
                        }

                        break;
                    case "Weekly":
                        if (radWeekly.Checked)
                        {
                            Globals.ArcFrequency = "Weekly";
                        }

                        break;
                    case "Monthly":
                        if (radMonthly.Checked)
                        {
                            Globals.ArcFrequency = "Monthly";
                        }

                        break;
                }
            }
            var xmlConfig = XDocument.Load(s);
            string sectionName = "LogSettings";
            var node = xmlConfig.Root.Element(sectionName).Element("LogLocation");
            if (node == null)
            {
                XElement el = new XElement("LogLocation");
                el.Value = txtLogLocation.Text;
                var newNode = xmlConfig.Root.Element(sectionName);
                newNode.Add(el);
            }
            else
            {
                node.Value = txtLogLocation.Text;
            }

            CheckDir(txtLogLocation.Text);

            sectionName = "SystemSettings";
            node = xmlConfig.Root.Element(sectionName).Element("ProfileLocation");
            if (node == null)
            {
                XElement el = new XElement("ProfileLocation");
                el.Value = txtProfileLocation.Text;
                var newNode = xmlConfig.Root.Element(sectionName);
                newNode.Add(el);
            }
            else
            {
                node.Value = txtProfileLocation.Text;
            }

            CheckDir(txtProfileLocation.Text);
            node = xmlConfig.Root.Element(sectionName).Element("AttachmentLocation");
            if (node == null)
            {
                XElement el = new XElement("AttachmentLocation");
                el.Value = txtAttLocation.Text;
                var newNode = xmlConfig.Root.Element(sectionName);
                newNode.Add(el);
            }
            else
            {
                node.Value = txtAttLocation.Text;
            }
            CheckDir(txtAttLocation.Text);
            node = xmlConfig.Root.Element(sectionName).Element("WinSCPLocation");
            if (node == null)
            {
                XElement el = new XElement("WinSCPLocation");
                el.Value = edWinSCP.Text;
                var newNode = xmlConfig.Root.Element(sectionName);
                newNode.Add(el);
            }
            else
            {
                node.Value = edWinSCP.Text;
            }

            node = xmlConfig.Root.Element(sectionName).Element("RunOnStart");
            if (node == null)
            {
                XElement el = new XElement("RunOnStart");
                el.Value = cbRunOnStart.Checked ? "true" : "false";
                var newNode = xmlConfig.Root.Element(sectionName);
                newNode.Add(el);
            }
            else
            {
                node.Value = cbRunOnStart.Checked ? "true" : "false";
            }
            if (node == null)
            {
                XElement el = new XElement("TimerInterval");
                el.Value = txtTimer.Value.ToString();
                var newNode = xmlConfig.Root.Element(sectionName);
                newNode.Add(el);
            }
            else
            {
                node.Value = txtTimer.Value.ToString();
            }
            sectionName = "ArchiveSettings";
            node = xmlConfig.Root.Element(sectionName).Element("ArchiveFrequency");
            if (node == null)
            {
                XElement el = new XElement("ArchiveFrequency");
                el.Value = Globals.ArcFrequency;
                var newNode = xmlConfig.Root.Element(sectionName);
                newNode.Add(el);
            }
            else
            {
                node.Value = Globals.ArcFrequency;
            }

            xmlConfig.Save(s);

            Globals.AppProfilePath = txtProfileLocation.Text;
            Globals.LogPath = txtLogLocation.Text;
            Globals.WinSCPPath = edWinSCP.Text;
            Globals.TimerInt = (int)txtTimer.Value;
            Globals.TempPath = txtAttLocation.Text;
            MessageBox.Show("System Settings Saved");

        }

        private void CheckDir(string text)
        {
            if (!Directory.Exists(text))
            {
                try
                {
                    Directory.CreateDirectory(text);
                }
                catch (IOException ex)
                {
                    MessageBox.Show(ex.GetType().Name + " error found. " + ex.Message);
                }

            }
        }

        private XElement UpdateNode(XElement parent, string elementName, string newVal)
        {
            var node = parent.Element(elementName).Element(elementName);
            if (node == null)
            {
                XElement el = new XElement(elementName);
                el.Value = newVal;
                node = parent;
                node.Add(el);
            }
            else
            {
                node.Value = newVal;
            }
            return node;
        }

        private void txtProfileLocation_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnAttLocation_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fd = new FolderBrowserDialog();
            fd.SelectedPath = txtAttLocation.Text;
            fd.ShowDialog();
            txtAttLocation.Text = fd.SelectedPath;
        }

        private void bbProfileLoc_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fd = new FolderBrowserDialog();
            fd.SelectedPath = txtProfileLocation.Text;
            fd.ShowDialog();
            txtProfileLocation.Text = fd.SelectedPath;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog fd = new OpenFileDialog();
            fd.FileName = edWinSCP.Text;
            fd.ShowDialog();
            edWinSCP.Text = fd.FileName;
        }

        private void bbHtmlTemplate_Click(object sender, EventArgs e)
        {
            OpenFileDialog fd = new OpenFileDialog();
            fd.FileName = edHtmlTemplate.Text;
            fd.ShowDialog();
            edHtmlTemplate.Text = fd.FileName;
        }
    }
}

