﻿using PopPrint.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Timers;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using Color = System.Drawing.Color;

namespace PopPrint
{
    public partial class frmMain : Form
    {
        public System.Timers.Timer tmrMain;
        Mailboxes mailboxProfiles;

        private void tmrMain_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (btnTimer.Text == "&Stop")
            {
                tmrMain.Stop();
                ProcessJobs();
                tmrMain.Start();
            }
            else
            {
                tmrMain.Stop();
            }
        }
        public frmMain()
        {
            InitializeComponent();
            slStatus.Text = "Timed Processes Stopped";
            tmrMain = new System.Timers.Timer();
        }


        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void settingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmSysProfile sysProfile = new frmSysProfile();
            sysProfile.ShowDialog();
            LoadSettings();
        }


        private void frmMain_Load(object sender, EventArgs e)
        {
            LoadSettings();
            if (Globals.RunOnStart)
            {
                btnTimer_Click(this.btnTimer, null);
            }
            dgLog.AutoGenerateColumns = false;
        }

        public void ProcessJobs()
        {
            var m = MethodInfo.GetCurrentMethod();
            Program.AddRTBText(rtbStatus, "Processing Jobs");
            // Load Mailbox List
            List<MailboxesMailbox> mailBoxList = mailboxProfiles.Items.ToList();
            if (mailBoxList != null)
            {
                foreach (MailboxesMailbox mailbox in mailBoxList)
                {
                    ProfileMailServer mserver = new ProfileMailServer();
                    mserver.Hostname = mailbox.PopServer;
                    mserver.Port = Convert.ToInt16(mailbox.PopPort);
                    mserver.UserName = mailbox.PopUserName;
                    mserver.Password = mailbox.PopPassword;
                    mserver.SSL = mailbox.PopSSL;
                    mserver.AddressTo = mailbox.AddressTo;
                    //Create list of Messages from Current mailbox. 
                    Program.AddRTBText(rtbStatus, String.Format("{0:g} ", DateTime.Now) + " Connecting to " + mserver.Hostname + " for mailbox : " + mserver.AddressTo);
                    try
                    {
                        var msgResult = MailModule.MessageCount(mserver.Hostname, mserver.Port, mserver.SSL, mserver.UserName, mserver.Password);
                        if (msgResult.Item2)
                        {
                            if (msgResult.Item1 > 0)
                            {
                                List<MsgSummary> msgList = MailModule.GetMail(mserver.Hostname, mserver.Port, mserver.SSL, mserver.UserName, mserver.Password);
                                Program.AddRTBText(rtbStatus, string.Format("{0:g} ", DateTime.Now) + msgResult.Item1 + " Messages found. Processing.", Color.Green);
                                foreach (MsgSummary msg in msgList)
                                {
                                    bool deleteMessage = false;
                                    if (msg.MsgFile != null)
                                    {
                                        var mbRule = GetMbRule(mailbox, msg);
                                        if (mbRule == null)
                                        {
                                            Program.AddRTBText(rtbStatus, String.Format("{0:g} ", DateTime.Now) + " Message is not valid: " + msg.MsgStatus + " in Message: (" + msg.MsgSubject + ")", Color.Red);
                                            continue;
                                            // No rules found for this email. Skip it and continue with next email.                                             
                                        }
                                        if (mbRule.PopAction == "Print" && string.IsNullOrEmpty(mbRule.PrintQueue))
                                        {
                                            PopPrintResources.WriteLog(string.Empty, PopPrintResources.EventCode.E405, m.Name, "No Print Queue specified", mbRule, msg);
                                            continue;

                                        }
                                        HtmlMsg htmlMsg = new HtmlMsg(msg);

                                        if (string.IsNullOrEmpty(htmlMsg.PdfPath))
                                        {

                                        }
                                        else
                                        {
                                            if (mbRule.PopAction == "Print")
                                            {
                                                var filePrinted = PrintDoc(htmlMsg.PdfPath, mbRule.PrintQueue);
                                                if (filePrinted.Item1)
                                                {
                                                    PopPrintResources.WriteLog(htmlMsg.PdfPath, PopPrintResources.EventCode.I400, m.Name, "File Printed.", mbRule, msg);
                                                    File.Delete(htmlMsg.PdfPath);
                                                }
                                                else
                                                {
                                                    PopPrintResources.WriteLog(htmlMsg.PdfPath, PopPrintResources.EventCode.E404, m.Name, filePrinted.Item2, mbRule, msg);
                                                }
                                                if (mbRule.PopDelete)
                                                {
                                                    deleteMessage = true;
                                                }
                                            }
                                        }

                                        foreach (MsgAtt att in msg.MsgFile)
                                        {
                                            //Remove the image01.jpg/png ie the signature images
                                            if (att.AttFilename.Extension.Contains("jpg") || att.AttFilename.Extension.Contains("png"))
                                            {
                                                if (att.AttFilename.Name.Contains("image"))
                                                {
                                                    if (File.Exists(att.AttFilename.FullName))
                                                    {
                                                        File.Delete(att.AttFilename.FullName);
                                                    }
                                                    // Other Image file exists. 
                                                    continue;
                                                }
                                            }
                                            switch (mbRule.PopAction)
                                            {
                                                case "FTP":
                                                    var res = SendFile(mbRule, att);
                                                    switch (res)
                                                    {
                                                        case "WINSCP":
                                                            MessageBox.Show("Location of WinSCP.EXE does not exist." + Environment.NewLine +
                                                                            "Please run setup to locate the Executable file", "WinSCP.EXE not found", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                                            PopPrintResources.WriteLog(att.AttFilename.FullName, PopPrintResources.EventCode.E309, m.Name, "WINSCP.EXE problem.", mbRule, msg);
                                                            break;
                                                        case "SUCCESS":
                                                            Program.AddRTBText(rtbStatus, string.Format("{0:g} ", DateTime.Now) + " File Transferred Ok: " + att.AttFilename + Environment.NewLine, Color.Green); deleteMessage = true;
                                                            PopPrintResources.WriteLog(att.AttFilename.FullName, PopPrintResources.EventCode.I300, m.Name, "File Transferred.", mbRule, msg);
                                                            break;
                                                        case "FAILED":
                                                            Program.AddRTBText(rtbStatus, string.Format("{0:g} ", DateTime.Now) + " File failed to transfer: " + att.AttFilename + Environment.NewLine, Color.Red);
                                                            PopPrintResources.WriteLog(att.AttFilename.FullName, PopPrintResources.EventCode.E302, m.Name, "File Failed to Transfer.", mbRule, msg);
                                                            break;
                                                        case "CONNECTION":
                                                            Program.AddRTBText(rtbStatus, string.Format("{0:g} ", DateTime.Now) + " Error establishing FTP Connection. Will try again. " + Environment.NewLine, Color.Red);
                                                            PopPrintResources.WriteLog(att.AttFilename.FullName, PopPrintResources.EventCode.E308, m.Name, "FTP Connection eror.", mbRule, msg);
                                                            break;

                                                    }


                                                    break;

                                                case "Print":

                                                    string fileType = att.AttFilename.Extension;
                                                    Program.AddRTBText(rtbStatus, string.Format("{0:g} ", DateTime.Now) + " Now Printing " + att.AttFilename.FullName, Color.Black);
                                                    //PrintMsg(msg, mbRule.PrintQueue);
                                                    var filePrinted = PrintDoc(att.AttFilename.FullName, mbRule.PrintQueue);
                                                    if (filePrinted.Item1)
                                                    {
                                                        Program.AddRTBText(rtbStatus, string.Format("{0:g} ", DateTime.Now) + " Printed Ok.", Color.Black);
                                                        PopPrintResources.WriteLog(att.AttFilename.FullName, PopPrintResources.EventCode.I400, m.Name, "File Printed Ok.", mbRule, msg);

                                                    }
                                                    else
                                                    {
                                                        Program.AddRTBText(rtbStatus, string.Format("{0:g} ", DateTime.Now) + " Printing Failed !!", Color.Red);
                                                        PopPrintResources.WriteLog(att.AttFilename.FullName, PopPrintResources.EventCode.E404, m.Name, filePrinted.Item2, mbRule, msg);
                                                        deleteMessage = false;
                                                    }
                                                    break;
                                                case "Save":
                                                    Program.AddRTBText(rtbStatus, string.Format("{0:g} ", DateTime.Now) + " Saving file for profile :" + mbRule.Name, Color.Black);
                                                    if (Directory.Exists(mbRule.SaveLocation))
                                                    {
                                                        try
                                                        {
                                                            att.AttFilename.MoveTo(Path.Combine(mbRule.SaveLocation, att.AttFilename.Name));
                                                            deleteMessage = true;
                                                        }
                                                        catch (IOException)
                                                        {
                                                            deleteMessage = false;
                                                        }

                                                    }
                                                    else
                                                    {

                                                    }
                                                    break;

                                            }
                                        }
                                    }

                                    if (msg.MsgStatus.Contains("Error"))
                                    {
                                        Program.AddRTBText(rtbStatus, String.Format("{0:g} ", DateTime.Now) + " Message Skipped due to Error: " + msg.MsgStatus + " in Message: (" + msg.MsgSubject + ")", Color.Red);
                                        deleteMessage = true;
                                    }

                                    if (deleteMessage)
                                    {
                                        MailModule.DeleteMessage(mserver.Hostname, mserver.Port, mserver.SSL, mserver.UserName, mserver.Password, msg.MsgId);
                                    }

                                }

                            }

                            else
                            {
                                Program.AddRTBText(rtbStatus, string.Format("{0:g} ", DateTime.Now) + " No Email found. Moving to next Profile.", Color.Gray);
                            }
                        }
                        else
                        {
                            Program.AddRTBText(rtbStatus, String.Format("{0:g} ", DateTime.Now) + " Warning. Unable to Connect to" + mserver.Hostname + " for mailbox : " + mserver.AddressTo + ". Check ErrorLog for details", Color.Red);
                        }
                    }
                    catch (Exception ex)
                    {
                        Program.AddRTBText(rtbStatus, String.Format("{0:g} ", DateTime.Now) + ex.GetType().Name + " Error found while checking " + mserver.Hostname + " for mailbox : " + mserver.AddressTo + " Error Message: " + ex.Message);
                    }
                }
            }
            else
            {
                Program.AddRTBText(rtbStatus, String.Format("{0:g} ", DateTime.Now) + " No profiles found. Processing stopped.");
                tmrMain.Stop();
                btnTimer.Text = "&Start";
                btnTimer.BackColor = Color.LightGreen;
                slStatus.Text = "Timed Processes Stopped";
            }

        }

        private Boolean PrintMsg(MsgSummary msg, string printQueue)
        {


            return true;
        }

        private string SendFile(MailboxesMailboxRule mbRule, MsgAtt att)
        {
            var ftpHelper = new FTPHelper(mbRule, att);
            return ftpHelper.SendFile();

        }

        private MailboxesMailboxRule GetMbRule(MailboxesMailbox mailbox, MsgSummary msg)
        {
            var ruleList = mailbox.Rule.ToList();
            var rule = (from x in ruleList
                        where (x.AddressFrom.ToLower() == msg.MsgFrom.ToLower() || x.AddressFrom == "*")
                      && (x.Subject == msg.MsgSubject || x.Subject == "*" || x.IsActive == true)
                        select x).FirstOrDefault();
            return rule;
        }




        public Boolean MsgValid(MsgSummary MsgtoValid, MsgAtt Attachment, String SenderEmail, String Subject, String FileType)
        {
            Boolean bValid = false;
            bool fromAddress = false;
            bool subject = false;

            // Cannot have both Subject AND email empty. However can have empty email address. 
            if (string.IsNullOrEmpty(SenderEmail) && !string.IsNullOrEmpty(Subject) || SenderEmail == "*")
            {
                fromAddress = true;
            }
            else
                // If email address contains Sender OR it matches completely
                if (MsgtoValid.MsgFrom.ToLower().Contains(SenderEmail.ToLower()) || MsgtoValid.MsgFrom.ToLower() == SenderEmail.ToLower())
            {
                fromAddress = true;
            }

            // If the Email address is valid and Subject is blank            
            if (string.IsNullOrEmpty(Subject) && fromAddress || SenderEmail == "*")
            {
                subject = true;
            }
            else
                // If Subject contains the Subject OR subject matches completely 
                if (MsgtoValid.MsgSubject.ToLower().Contains(Subject.ToLower()) || MsgtoValid.MsgSubject.ToLower() == Subject.ToLower() || Subject == "*")
            {
                subject = true;
            }

            if (fromAddress && subject)
            {
                bValid = true;
            }
            else
            {
                bValid = false;
                Program.AddRTBText(rtbStatus, string.Format("{0:g} ", DateTime.Now) + " Unable to match Receive profile:  Sender:" + SenderEmail + " Subject:" + Subject, Color.Red);
            }
            return bValid;
        }



        public Tuple<bool, string> PrintDoc(string filetoPrint, string varPrintTo)
        {
            var m = MethodInfo.GetCurrentMethod();
            string err = string.Empty;
            bool result = false;
            FileInfo fi = new FileInfo(filetoPrint);
            var printDoc = new Print(varPrintTo);
            string[] docExts = { ".doc", ".docx", ".xls", ".xlsx", ".txt", ".csv" };
            string[] imgExts = { ".jpg", ".png", ".bmp", ".jpeg" };

            if (fi.Extension.ToLower() == ".pdf")
            {
                result = printDoc.PrintPdf(filetoPrint);
            }
            else
            {
                if (docExts.Contains(fi.Extension.ToLower()))
                {
                    result = printDoc.PrintDoc(filetoPrint);
                }
                else
                {
                    if (imgExts.Contains(fi.Extension.ToLower()))
                    {
                        if (File.Exists(filetoPrint))
                        {
                            result = printDoc.PrintImagePage(filetoPrint);
                        }
                    }
                    else
                    {
                        return new Tuple<bool, string>(false, "File type " + Path.GetExtension(filetoPrint) + " not currently supported in printing function");
                    }
                }
            }


            if (!result)
            {
                err = printDoc.ErrLog;
            }

            try
            {
                if (File.Exists(filetoPrint))
                {
                    File.Delete(filetoPrint);
                }
            }
            catch (IOException ex)
            {
                err = ex.GetType().Name + " " + ex.Message;
            }

            return new Tuple<bool, string>(result, err);

        }


        public void LoadSettings()
        {
            string s = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name + "-CONFIG.XML";
            string appPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "PopPrint");
            bool setupNeeded = false;

            s = Path.Combine(appPath, s);
            tmrMain.Elapsed += new ElapsedEventHandler(tmrMain_Elapsed);
            XmlDocument xmlConfig = new XmlDocument();
            if (!File.Exists(s))
            {
                CreateConfigXML(s);

            }
            xmlConfig.Load(s);

            XmlNodeList nodelist;
            nodelist = xmlConfig.SelectNodes("/PopPrint/SystemSettings");
            if (nodelist == null)
            {
                CreateConfigXML(s);
            }
            else
            {

                XmlNode xmlProfileSettings = nodelist[0].SelectSingleNode("ProfileLocation");
                if (!String.IsNullOrEmpty(xmlProfileSettings.InnerText))
                {
                    Globals.AppProfilePath = xmlProfileSettings.InnerText;
                    if (!Directory.Exists(Globals.AppProfilePath))
                    {
                        setupNeeded = true;
                    }
                }
                else
                {
                    string path = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
                    if (!Directory.Exists(Path.Combine(path, "POPPrint")))
                    {
                        Directory.CreateDirectory(Path.Combine(path, "POPPrint"));

                    }
                    appPath = Path.Combine(path, "POPPrint");
                    Globals.AppProfilePath = appPath;
                    xmlProfileSettings.InnerText = appPath;
                }
                XmlNode xmlTempSettings = nodelist[0].SelectSingleNode("AttachmentLocation");
                if (!string.IsNullOrEmpty(xmlTempSettings.InnerText))
                {
                    Globals.TempPath = xmlTempSettings.InnerText;
                    if (!Directory.Exists(Globals.TempPath))
                    {
                        setupNeeded = true;
                    }
                }
                else
                {
                    if (!Directory.Exists(Path.Combine(appPath, "Temp")))
                    {
                        try
                        {
                            Directory.CreateDirectory(Path.Combine(appPath, "Temp"));

                        }
                        catch (IOException ex)
                        {
                            DialogResult dr = new DialogResult();

                            dr = MessageBox.Show("Unable to Create Application Temp folder. Error was :" + ex.Message, "Creating System Folders", MessageBoxButtons.RetryCancel, MessageBoxIcon.Warning);
                            if (dr == DialogResult.Retry)
                            {

                            }
                        }
                    }
                    Globals.TempPath = Path.Combine(appPath, "Temp");
                    xmlTempSettings.InnerText = Globals.TempPath;
                }
                XmlNode xmlWinScpLocation = nodelist[0].SelectSingleNode("WinSCPLocation");
                if (xmlWinScpLocation == null)
                {
                    setupNeeded = true;
                }
                else
                {
                    Globals.WinSCPPath = xmlWinScpLocation.InnerText;
                }
                XmlNode xmlRunOnStart = nodelist[0].SelectSingleNode("RunOnStart");
                if (xmlRunOnStart != null)
                {
                    bool tryNull;
                    if (bool.TryParse(xmlRunOnStart.InnerText, out tryNull))
                    {
                        Globals.RunOnStart = bool.Parse(xmlRunOnStart.InnerText);
                    }
                }
                Globals.ProfileXml = Path.Combine(Globals.AppProfilePath, "PopPrintProfiles.xml");

                XmlNode xmlTimer = nodelist[0].SelectSingleNode("/PopPrint/TimerInterval");
                if (xmlTimer != null)
                {
                    Globals.TimerInt = Convert.ToInt16(xmlTimer.InnerText);
                }
                else
                {
                    XmlNode timernode = xmlConfig.SelectSingleNode("/PopPrint");
                    XmlElement timerElement = xmlConfig.CreateElement("TimerInterval");
                    timernode.AppendChild(timerElement);
                    timerElement.InnerText = "5";
                    Globals.TimerInt = 5;
                }
                tmrMain.Interval = (Globals.TimerInt * 1000) * 60;

                nodelist = xmlConfig.SelectNodes("/PopPrint/ArchiveSettings");
                XmlNode xmlArchiveSettings = nodelist[0].SelectSingleNode("ArchiveFrequency");
                if (!String.IsNullOrEmpty(xmlArchiveSettings.InnerText))
                {
                    Globals.ArcFrequency = xmlArchiveSettings.InnerText;
                }
                else
                {
                    Globals.ArcFrequency = "Daily";
                    xmlArchiveSettings.InnerText = "Daily";
                }


                nodelist = xmlConfig.SelectNodes("/PopPrint/LogSettings");
                XmlNode xmlLogSettings = nodelist[0].SelectSingleNode("LogLocation");
                if (!String.IsNullOrEmpty(xmlLogSettings.InnerText))
                {
                    Globals.LogPath = xmlLogSettings.InnerText;
                    if (!Directory.Exists(Globals.LogPath))
                    {
                        setupNeeded = true;
                    }
                }
                else
                {

                    Globals.TempPath = Path.Combine(appPath, "Temp");
                    if (!Directory.Exists(Path.Combine(appPath, "Log")))
                    {
                        try
                        {
                            Directory.CreateDirectory(Path.Combine(appPath, "Log"));

                        }
                        catch (IOException ex)
                        {
                            DialogResult dr = new DialogResult();

                            dr = MessageBox.Show("Unable to Create Application Log folder. Error was :" + ex.Message, "Creating System Folders", MessageBoxButtons.RetryCancel, MessageBoxIcon.Warning);
                            if (dr == DialogResult.Retry)
                            {

                            }

                        }
                    }
                    Globals.LogPath = Path.Combine(appPath, "Log");
                }


                xmlConfig.Save(s);
                if (setupNeeded)
                {
                    MessageBox.Show("There was an error locating one or more of the System Folders. \r\n Please run setup to confirm the folders are correct", "Error Loading System Settings", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    btnTimer.Enabled = false;
                }
                else
                {
                    btnTimer.Enabled = true;
                    LoadProfiles(Globals.ProfileXml);
                }



            }


            //dsProfiles.ReadXml(Globals.ProfileXml);
        }

        private void LoadProfiles(string profileXml)
        {
            if (!File.Exists(profileXml))
            {
                CreateProfileXML(profileXml);

            }
            try
            {
                using (FileStream fs = new FileStream(profileXml, FileMode.Open, FileAccess.Read))
                {
                    XmlSerializer xs = new XmlSerializer(typeof(Mailboxes));
                    mailboxProfiles = (Mailboxes)xs.Deserialize(fs);
                    fs.Close();
                }

            }
            catch (IOException ex)
            {
                var err = ex.GetType().Name + " " + ex.Message + Environment.NewLine + ex.InnerException;
                //    MessageBox.Show("Missing Profile Location. Please locate file", "Error loading Profile XML", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                //    frmSysProfile systemSettings = new frmSysProfile();
                //    systemSettings.ShowDialog();
                //    LoadSettings();
                //
            }
            catch (InvalidOperationException ex)
            {
                var err = ex.GetType().Name + " " + ex.Message + Environment.NewLine + ex.InnerException;
                MessageBox.Show("There was an error loading the Profiles. Please check EACH of the Profiles before starting", "Error loading Profiles", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            catch (Exception ex)
            {
                var err = ex.GetType().Name + " " + ex.Message + Environment.NewLine + ex.InnerException;
                CreateProfileXML(Globals.ProfileXml);
            }
        }

        public void CreateProfileXML(string appprofilexml)
        {
            //First create the XDocument instance
            // Note that the Xdocument is created in one line. I have separated it to make it easier to read
            XDocument xmlProfileConf = new XDocument(
                new XDeclaration("1.0", "UTF-8", "Yes"),
                new XElement("Mailboxes",
                    new XElement("Mailbox",
                     new XElement("Rule",
                                        new XElement("Name"),
                                        new XElement("IsActive"),
                                        new XElement("PopAction"),
                                        new XElement("PopDelete"),
                                        new XElement("ArchiveFile"),
                                        new XElement("ArchiveLocation"),
                                        new XElement("AddressFrom"),
                                        new XElement("Subject"),
                                        new XElement("SaveLocation"),
                                        new XElement("SaveDomain"),
                                        new XElement("SaveUser"),
                                        new XElement("SavePassword"),
                                        new XElement("PrintQueue"),
                                        new XElement("FtpServer"),
                                        new XElement("FtpPort"),
                                        new XElement("FtpUserName"),
                                        new XElement("FtpPassword"),
                                        new XElement("FtpKeyPath"),
                                        new XElement("FtpFingerPrint"),
                                        new XElement("FtpSecure"),
                                        new XElement("FtpSSLCertifcate"),
                                        new XElement("FtpFolder")
                                                   ),
                        new XElement("PopServer"),
                        new XElement("PopUserName"),
                        new XElement("PopPassword"),
                        new XElement("PopPort"),
                        new XElement("PopSSL"),
                        new XElement("AddressTo")

                                     )));

            while (File.Exists(appprofilexml))
            {
                File.Delete(appprofilexml);
            }

            xmlProfileConf.Save(appprofilexml);

            LoadProfiles(appprofilexml);
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmAbout about = new frmAbout();
            about.ShowDialog();
        }

        private void btnTimer_Click(object sender, EventArgs e)
        {
            var m = MethodInfo.GetCurrentMethod();
            if (((Button)sender).Text == "&Start")
            {
                ((Button)sender).Text = "&Stop";
                ((Button)sender).BackColor = Color.LightCoral;
                PopPrintResources.WriteLog(string.Empty, PopPrintResources.EventCode.E001, m.Name, "Start button pressed.", null, null);
                slStatus.Text = "Timed Processes Started";
                ProcessJobs();
                //  PrintJobs("");
                tmrMain.Start();

            }
            else
            {
                ((Button)sender).Text = "&Start";
                ((Button)sender).BackColor = Color.LightGreen;
                slStatus.Text = "Timed Processes Stopped";
                PopPrintResources.WriteLog(string.Empty, PopPrintResources.EventCode.E002, m.Name, "Stop Button Pressed.", null, null);
            }
        }

        public void CreateConfigXML(string appconfigxml)
        {
            //First create the XDocument instance
            // Note that the Xdocument is created in one line. I have separated it to make it easier to read
            XDocument xmlSysConfig = new XDocument(
                new XDeclaration("1.0", "UTF-8", "Yes"),
                new XElement("PopPrint",
                new XElement("LogSettings",
                new XElement("LogLocation")),
                new XElement("SystemSettings",
                new XElement("ProfileLocation"),
                new XElement("AttachmentLocation")),
                new XElement("ArchiveSettings",
                new XElement("ArchiveFrequency")))
                );
            xmlSysConfig.Save(appconfigxml);
        }

        private void jobProfilesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmJobProfiles jobProfiles = new frmJobProfiles();
            jobProfiles.Show();
        }


        private void tcPopPrint_Selected(object sender, TabControlEventArgs e)
        {
            if (e.TabPage == tabLog)
            {
                try
                {
                    using (XmlLogfileStream logFileStream = new XmlLogfileStream(Path.Combine(Globals.LogPath, "PopPrintLog.XML")))
                    {
                        DataSet ds = new DataSet();

                        ds.ReadXml(logFileStream);
                        DataView dv = new DataView(ds.Tables[0]);
                        dv.Sort = "Time ASC";
                        dgLog.DataSource = dv.ToTable();
                        //dgLog.DataMember = ds.Tables[0].TableName;
                        //ds.Tables[0].DefaultView.Sort = "Time DESC";



                    }
                    foreach (DataGridViewColumn dc in dgLog.Columns)
                    {
                        dc.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;

                    }
                }
                catch (Exception)
                {

                }

            }
        }

        private void clearLogToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult dr = new DialogResult();
            dr = MessageBox.Show("Are you sure you want to clear the Log file", "Clear Log File", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (dr == DialogResult.Yes)
            {
                string logFile = Path.Combine(Globals.LogPath, "PopPrintLog.XML");
                using (StreamWriter w = new StreamWriter(logFile))
                {
                    w.Write(string.Empty);
                }

            }
        }
    }
}
