﻿using System.Reflection;
using WinSCP;

namespace PopPrint.Classes
{
    public class FTPHelper
    {
        #region Members
        MailboxesMailboxRule _mbRule;
        MsgAtt _att;

        #endregion

        #region Properties
        public MailboxesMailboxRule MbRule
        {
            get { return _mbRule; }
            set { _mbRule = value; }
        }
        public MsgAtt Att
        {
            get
            {
                return _att;
            }
            set
            {
                _att = value;
            }
        }
        public string ErrorLog { get; set; }
        #endregion

        #region Constructors
        public FTPHelper(MailboxesMailboxRule rule, MsgAtt msgAtt)
        {
            MbRule = rule;
            Att = msgAtt;
        }
        #endregion

        #region Methods

        public string SendFile()
        {
            return TransferFile(CreateSession());
        }
        private SessionOptions CreateSession()
        {
            SessionOptions sessionOptions = new SessionOptions
            {
                HostName = _mbRule.FtpServer,
                PortNumber = int.Parse(_mbRule.FtpPort),
                UserName = _mbRule.FtpUserName,
                Password = _mbRule.FtpPassword
            };
            if (_mbRule.FtpSecure)
            {
                sessionOptions.Protocol = Protocol.Sftp;

                sessionOptions.SshHostKeyFingerprint = _mbRule.FtpFingerprint;
                if (!string.IsNullOrEmpty(_mbRule.FtpSSLCertificate))
                {
                    sessionOptions.SshPrivateKeyPath = _mbRule.FtpSSLCertificate;
                }
            }
            else
            {
                sessionOptions.Protocol = Protocol.Ftp;
            }
            return sessionOptions;
        }

        private string TransferFile(SessionOptions sessionOptions)
        {
            MethodBase m = MethodBase.GetCurrentMethod();
            try
            {
                TransferOperationResult tr;

                using (Session session = new Session())
                {

                    session.ExecutablePath = Globals.WinSCPPath;
                    session.Open(sessionOptions);
                    if (session.Opened)
                    {
                        TransferOptions tOptions = new TransferOptions();
                        tOptions.TransferMode = TransferMode.Automatic;
                        tr = session.PutFiles(_att.AttFilename.FullName, _mbRule.FtpFolder, false, tOptions);
                        if (tr.IsSuccess)
                        {
                            PopPrintResources.WriteLog(_att.AttFilename.FullName, PopPrintResources.EventCode.E000, m.Name, "File Transferred Ok.", null, null);
                            return "SUCCESS";

                        }
                        else
                        {
                            PopPrintResources.WriteLog(_att.AttFilename.FullName, PopPrintResources.EventCode.E307, m.Name, "Unable to Send file.", null, null);
                           
                            return "FAILED";
                        }

                    }
                    else
                    {
                        
                        PopPrintResources.WriteLog(_att.AttFilename.FullName, PopPrintResources.EventCode.E308, m.Name, "Error Connecting to Server.", null, null);
                        return "CONNECTION";
                    }
                }

            }
            catch (SessionRemoteException ex)
            {
                PopPrintResources.WriteLog(_att.AttFilename.FullName, PopPrintResources.EventCode.E301, m.Name, ex.Message, null, null);
                return "EXCEPTION";
            }
        }
        #endregion

        #region Helpers
        #endregion


    }
}
