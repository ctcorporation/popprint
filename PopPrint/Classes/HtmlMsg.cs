﻿using PdfSharp;
using PdfSharp.Pdf;
using PopPrint.Interfaces;
using System;
using System.IO;
using System.Reflection;
using System.Text.RegularExpressions;
using TheArtOfDev.HtmlRenderer.PdfSharp;

namespace PopPrint.Classes
{
    public class HtmlMsg
    {
        private MsgSummary _msg;
        public IMsgSummary Msg { get; set; }
        public string PdfPath { get; set; }
        public HtmlMsg(IMsgSummary msg)
        {
            Msg = msg;
            CreateHtml();

        }

        private string GetData(string opt)
        {
            var attachmentNames = string.Empty;
            foreach (var att in Msg.MsgFile)
            {
                attachmentNames += att.AttFilename.Name + "<br />";
            }
            using (var reader = new StreamReader("DefaultMailTemplate.html"))
            {
                return reader
                .ReadToEnd()
                .Replace("@MsgDate@", Msg.MsgDate.ToString("dd/MM/yyyy HH:mm:ss"))
                .Replace("@Subject@", Msg.MsgSubject)
                .Replace("@Recipient@", Msg.MsgTo)
                .Replace("@cc@", Msg.MsgCC)
                .Replace("@bcc@", Msg.MsgBcc)
                .Replace("@Sender@", Msg.MsgFrom)
                .Replace("@MsgBody@", GetBody(opt))
                .Replace("@Attachments@", attachmentNames);
            }
        }

        private void CreateHtml()
        {
            var m = MethodInfo.GetCurrentMethod();
            PdfDocument pdf;
            try
            {
                pdf = PdfGenerator.GeneratePdf(GetData("H"), PageSize.A4);
            }
            catch (Exception ex)
            {
                PopPrintResources.WriteLog(string.Empty, PopPrintResources.EventCode.E201, m.Name, "HTML Format Error:" + ex.Message, null, (MsgSummary)Msg);
                pdf = PdfGenerator.GeneratePdf(GetData("T"), PageSize.A4);
            }
            if (pdf != null)
            {
                PdfPath = Path.Combine(Path.GetTempPath(), Msg.MsgId.ToString() + ".PDF");
                int i = 1;
                while (File.Exists(PdfPath))
                {
                    PdfPath = Path.Combine(Path.GetTempPath(), Msg.MsgId.ToString() + '-' + i.ToString() + ".PDF");
                    i++;
                }
                pdf.Save(PdfPath);
            }


        }

        private string GetBody(string opt)
        {

            Regex regex = new Regex(@"(\r\n|\r|\n)+");
            var txtBody = Msg.MsgBody!=null?regex.Replace(Msg.MsgBody, "<br />"):string.Empty;
            var htmlBody = string.IsNullOrEmpty(Msg.MsgHtmlBody) ? txtBody : Msg.MsgHtmlBody;
            if (opt == "T")
            {
                return "[Message will be shown in Plain Text] <BR />" + txtBody;
            }
            else
            { return htmlBody; }



        }
    }
}
