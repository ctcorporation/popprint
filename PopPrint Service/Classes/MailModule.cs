﻿using OpenPop.Pop3;
using OpenPop.Pop3.Exceptions;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Net.Mail;
using System.Reflection;

namespace PopPrint_Service.Classes
{
    class MailModule
    {

        public static Tuple<int, bool> MessageCount(string hostname, int port, string useSsl, string username, string password)
        {
            var m = MethodBase.GetCurrentMethod();
            bool connection = false;
            int msgCount = 0;
            bool ssl = false;
            switch (useSsl)
            {
                case "none":
                    ssl = false;
                    break;

                case "SSL":
                    ssl = true;
                    break;

                case "TLS":
                    ssl = true;
                    break;

            }
            List<MsgSummary> allMessages = new List<MsgSummary>();
            using (Pop3Client client = new Pop3Client())
            {
                try
                {
                    client.Connect(hostname, port, ssl);
                    if (client.Connected)
                    {
                        client.Authenticate(username, password);
                        connection = true;
                        msgCount = client.GetMessageCount();
                    }
                }
                catch (PopServerNotAvailableException ex)
                {
                    PopPrintResources.WriteLog(string.Empty, PopPrintResources.EventCode.E302, m.Name, ex.Message, null, null);


                }
            }


            var result = new Tuple<int, bool>(msgCount, connection);
            return result;
        }

        public static List<MsgSummary> GetMail(string hostname, int port, string useSsl, string username, string password)
        {
            int messageCount = 0;
            String fromMail = "";
            String subject = "";
            bool ssl = false;
            switch (useSsl)
            {
                case "none":
                    ssl = false;
                    break;

                case "SSL":
                    ssl = true;
                    break;

                case "TLS":
                    ssl = true;
                    break;

            }
            List<MsgSummary> allMessages = new List<MsgSummary>();
            Pop3Client client = new Pop3Client();
            try
            {
                client.Connect(hostname, port, ssl);
                if (client.Connected)
                {
                    client.Authenticate(username, password);
                }
                messageCount = client.GetMessageCount();
                if (messageCount > 0)
                {
                    int sessioncount = 0;
                    if (messageCount > 50)
                    {
                        sessioncount = 50;
                    }
                    else
                    {
                        sessioncount = messageCount;
                    }
                    for (int i = sessioncount; i > 0; i--)
                    {
                        if (client.GetMessage(i) != null)
                        {
                            OpenPop.Mime.Message msg = client.GetMessage(i);
                            MsgSummary newSumm = new MsgSummary();
                            try
                            {
                                if (msg.Headers.From.MailAddress.Address != null && msg.Headers.From.MailAddress.Address != "")
                                {
                                    fromMail = msg.Headers.From.MailAddress.Address;
                                }
                                if (msg.Headers.Subject != null && msg.Headers.Subject != "")
                                {
                                    subject = msg.Headers.Subject;
                                }
                                List<MsgAtt> att = new List<MsgAtt>();
                                foreach (OpenPop.Mime.MessagePart attach in msg.FindAllAttachments())
                                {
                                    string file_name_attach = attach.FileName;
                                    if (attach.FileName != "(no name)")
                                    {
                                        FileInfo fi = new FileInfo(Path.Combine(Globals.TempPath, file_name_attach));
                                        int ij = 0;
                                        string sName = fi.Name;
                                        string sExt = fi.Extension;

                                        while (File.Exists(sName))
                                        {
                                            sName = fi.Name + ij.ToString() + sExt;
                                            ij++;

                                        }
                                        fi = new FileInfo(Path.Combine(Globals.TempPath, sName));
                                        att.Add(new MsgAtt() { AttFilename = fi });

                                        attach.Save(fi);
                                    }

                                }
                                newSumm.MsgFile = att;
                                newSumm.MsgCC = string.Join(",", msg.Headers.Cc);
                                newSumm.MsgBcc = string.Join(",", msg.Headers.Bcc);
                                newSumm.MsgFrom = fromMail;
                                newSumm.MsgTo = string.Join(",", msg.Headers.To);
                                newSumm.MsgSubject = subject;
                                newSumm.MsgDate = msg.Headers.DateSent;
                                var txtBody = msg.FindFirstPlainTextVersion();
                                if (txtBody != null)
                                {
                                    newSumm.MsgBody = txtBody.GetBodyAsText();
                                }
                                var htmlBody = msg.FindFirstHtmlVersion();
                                if (htmlBody != null)
                                {
                                    newSumm.MsgHtmlBody = htmlBody.GetBodyAsText();

                                }

                                newSumm.MsgStatus = "Ok";
                                newSumm.MsgId = i;


                            }
                            catch (ArgumentException exArgs)
                            {
                                if (exArgs.Message.ToUpper().Contains("ILLEGAL"))
                                {
                                    client.DeleteMessage(i);
                                }

                                newSumm.MsgFrom = fromMail;
                                newSumm.MsgSubject = subject;
                                newSumm.MsgStatus = "Error: " + exArgs.Message;
                                newSumm.MsgId = i;
                            }
                            catch (Exception exAtt)
                            {
                                // Program.AddRTBText(rtbStatus, string.Format("{0:g} ", DateTime.Now) + " Error Retrieving Email message " + i.ToString() + " of " + messageCount.ToString()+ " Error was: " + exAtt.Message + Environment.NewLine, Color.Red);
                                newSumm.MsgStatus = exAtt.Message;
                                newSumm.MsgFrom = fromMail;
                                newSumm.MsgSubject = subject;
                                newSumm.MsgStatus = "Error: " + exAtt.Message;
                                newSumm.MsgId = i;
                            }
                            finally
                            {
                                allMessages.Add(newSumm);
                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                MsgSummary newSumm = new MsgSummary();
                newSumm.MsgStatus = ex.Message;
                allMessages.Add(newSumm);
            }
            finally
            {
                try
                {
                    client.Disconnect();
                }
                catch (Exception)
                {

                }
            }
            return allMessages;

        }

        public static string CheckMail(string hostname, int port, bool useSsl, string username, string password)
        {
            string mailStatus = "";
            Pop3Client client = new Pop3Client();
            try
            {
                client.Connect(hostname, port, useSsl);
                if (client.Connected)
                {
                    mailStatus += "Server " + hostname + ":" + port.ToString() + ", Connected. Ok." + Environment.NewLine;
                    try
                    {

                        client.Authenticate(username, password);
                        mailStatus += "Username and Password details are Correct. Mail Connection Connected. Ok." + Environment.NewLine;
                    }
                    catch (Exception)
                    {
                        mailStatus += "Username and/or Password are incorrect. Please confirm the credentials" + Environment.NewLine;
                    }


                }
                else
                {
                    mailStatus += "Warning. Server " + hostname + ":" + port.ToString() + ", Not Connected. " + Environment.NewLine + "Please check server, port, SSL settings";
                }
            }
            catch (Exception ex)
            {
                mailStatus = ex.Message;
            }
            finally
            {
                try
                {
                    client.Disconnect();
                    mailStatus += "Server disconnected." + Environment.NewLine;
                }
                catch (Exception)
                {

                }
            }
            return mailStatus;


        }

        public static void SendCompletedCallback(object sender, AsyncCompletedEventArgs e)
        {
            String token = (string)e.UserState;

            if (e.Cancelled)
            {
                Console.WriteLine("[{0}] Send canceled.", token);
            }
            if (e.Error != null)
            {
                Console.WriteLine("[{0}] {1}", token, e.Error.ToString());
            }
            else
            {
                Console.WriteLine("Message sent.");
            }
        }

        public static void DeleteMessage(string hostname, int port, string useSsl, string username, string password, int msgId)
        {
            int messageCount = 0;
            bool ssl = false;
            switch (useSsl)
            {
                case "none":
                    ssl = false;
                    break;

                case "SSL":
                    ssl = true;
                    break;

                case "TLS":
                    ssl = true;
                    break;

            }
            Pop3Client client = new Pop3Client();
            try
            {
                client.Connect(hostname, port, ssl);
                if (client.Connected)
                {
                    client.Authenticate(username, password);
                }
                messageCount = client.GetMessageCount();
                if (messageCount > 0)
                {

                    client.DeleteMessage(msgId);
                    client.Disconnect();
                }
            }
            catch (Exception)
            {

            }
        }

        public static String sendMsg(String file, String to, String mySubject, String msg)
        {
            SmtpClient myMail = new SmtpClient();
            //TODO

            //myMail.Host = SMTPServer.Server;
            //myMail.Port = Convert.ToInt16(SMTPServer.Port);
            //myMail.EnableSsl = ssl;
            //myMail.Credentials = new System.Net.NetworkCredential(SMTPServer.Username, SMTPServer.Password);
            myMail.DeliveryMethod = SmtpDeliveryMethod.Network;
            MailMessage myMsg = new MailMessage();
            myMsg.To.Add(new System.Net.Mail.MailAddress(to));
            if (file != "")
            {
                try
                {
                    if (File.Exists(file))
                    {
                        myMsg.Attachments.Add(new System.Net.Mail.Attachment(file));

                    }
                }
                catch (Exception)
                {
                    //   sendMsg("", Globals.AlertsTo, "Error Sending Alert Email", "Error Message is: " + ex.Message + Environment.NewLine + "Original Message to send was: " + Environment.NewLine + msg);
                }
            }
            //  myMsg.From = new System.Net.Mail.MailAddress(SMTPServer.Email, "CTC Adapter");
            myMsg.Subject = mySubject;
            myMsg.Body = msg;
            myMail.SendCompleted += new SendCompletedEventHandler(SendCompletedCallback);
            //String userState = "Send from CTCNODE";
            try
            {
                myMail.Send(myMsg);
                myMsg.Dispose();
                myMail.Dispose();

                return "Message Sent OK";
            }
            catch (Exception ex)
            {
                myMsg.Dispose();
                myMail.Dispose();
                return "Message failed to send. Error was: " + ex.Message;
            }
        }


    }
}
