﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Xml.Linq;

namespace PopPrint_Service.Classes
{
    public class SystemSettings
    {


        #region members
        private string _xmlPath;
        private string _popPrintRootPath;
        private XDocument xmlSettings;
        #endregion

        #region properties
        public string XmlPath
        {
            get { return _xmlPath; }
            set { _xmlPath = value; }
        }

        #endregion

        #region constructors
        public SystemSettings(string xmlPath)
        {
            _xmlPath = xmlPath;
            _popPrintRootPath = Path.GetDirectoryName(xmlPath);
            if (!File.Exists(_xmlPath))
            {
                if (!Directory.Exists(_popPrintRootPath))
                {
                    Directory.CreateDirectory(_popPrintRootPath);
                }
                CreateDefaultSettings();
            }
            xmlSettings = XDocument.Load(_xmlPath);
        }

        private void CreateDefaultSettings()
        {
            XDocument xmlConf = new XDocument(
                         new XDeclaration("1.0", "UTF-8", "Yes"),
                         new XElement("PopPrint",
                         new XElement("LogSettings",
                             new XElement("LogLocation")),
                         new XElement("SystemSettings",
                             new XElement("ProfileLocation"),
                             new XElement("AttachmentLocation"),
                             new XElement("WinSCPLocation"),
                             new XElement("RunOnStart")),
                         new XElement("ArchiveSettings",
                             new XElement("ArchiveFrequency"))));
            xmlConf.Save(_xmlPath);
        }
        #endregion

        #region Helpers
        public XElement GetElement(string elementName, string valueToFind)
        {
            try
            {
                if (!xmlSettings.Descendants(elementName).Elements(valueToFind).Any())
                {
                    return null;
                }

                var node = xmlSettings.Descendants(elementName).Elements(valueToFind).FirstOrDefault();
                if (node.Name == valueToFind)
                {
                    if (string.IsNullOrEmpty(node.Value))
                    {
                        return null;
                    };
                    return node;
                }
                return null;
            }
            catch (Exception ex)
            {
                var st = new StackTrace();
                var sf = st.GetFrame(0);

                var currentMethodName = sf.GetMethod().Name;
                Console.WriteLine("Error returning value: Parent='" + elementName + "' NodeToFind='" + valueToFind + "'");

                return null;
            }

        }

        public void SetElement(string parentElement, string newElement, string newValue)
        {
            try
            {
                var xpath = xmlSettings.Root;
                if (!xmlSettings.Descendants(parentElement).Elements(newElement).Any())
                {
                    XElement newEl = new XElement(newElement);
                    newEl.Value = newValue;
                    var parent = (from e in xmlSettings.Descendants(parentElement)
                                  select e).FirstOrDefault();
                    parent.Add(newEl);
                }
                else
                {
                    var parent = xmlSettings.Descendants(parentElement).Elements(newElement).FirstOrDefault();
                    parent.Value = newValue;
                }
            }
            catch (Exception ex)
            {
                var st = new StackTrace();
                var sf = st.GetFrame(0);

                var currentMethodName = sf.GetMethod().Name;
                Console.WriteLine("Error returning value: Parent='" + parentElement + "' NodeToFind='" + newElement + "'");

            }

        }
        public void UpdateSettings()
        {
            xmlSettings.Save(_xmlPath);
        }

        #endregion

    }
}
