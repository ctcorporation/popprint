﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Printing;
using System.IO;

namespace PopPrint_Service.Classes
{ 
    public class Print
    {
        #region Members
        private System.Drawing.Font printFont;
        private StreamReader streamToPrint;

        static string filePath;
        private Image _img;

        private string _printQueue;
        #endregion


        #region Properties
        public string PrintQueue
        {
            get
            { return _printQueue; }
            set
            { _printQueue = value; }
        }

        public string ErrLog { get; set; }
        public string ImgFile { get; set; }
        #endregion



        #region Constructors

        public Print(string printQueue)
        {
            PrintQueue = printQueue;
        }
        #endregion

        #region Methods
        public bool PrintImagePage(string fileToPrint)
        {
            try
            {
                _img = Image.FromFile(fileToPrint);
                PrintDocument printImage = new PrintDocument();
                printImage.PrintPage += new PrintPageEventHandler(pdImage_PrintPage);
                printImage.PrinterSettings.PrinterName = _printQueue;
                printImage.Print();
                return true;
            }
            catch (Exception ex)
            {
                ErrLog += "Unable to Print Image file " + fileToPrint + ". " + ex.Message;
                return false;
            }

        }

        private void pdImage_PrintPage(object sender,
   PrintPageEventArgs e)
        {
            // Print in the upper left corner at its full size.

            System.Drawing.Rectangle m = e.PageBounds;

            if ((double)_img.Width / (double)_img.Height > (double)m.Width / (double)m.Height) // image is wider
            {
                m.Height = (int)((double)_img.Height / (double)_img.Width * (double)m.Width);
            }
            else
            {
                m.Width = (int)((double)_img.Width / (double)_img.Height * (double)m.Height);
            }
            e.Graphics.DrawImage(_img, m);

        }

     
        public bool PrintDoc(string fileToPrint)
        {
            bool result = false;
            if (File.Exists(fileToPrint))
            {
                var fi = new FileInfo(fileToPrint);
                int iFile = 1;
                string newPdfName = Path.GetFileNameWithoutExtension(fileToPrint);
                if (File.Exists(Path.Combine(Globals.TempPath, newPdfName + ".PDF")))
                {
                    File.Delete(Path.Combine(Globals.TempPath, newPdfName + ".PDF"));
                }
                string cmdLine = string.Format("--convert-to pdf --nologo --headless --outdir \"{1}\" \"{0}\"", fileToPrint, Globals.TempPath);
                try
                {
                    string libOfficePath = GetLibOfficePath();
                    if (string.IsNullOrEmpty(libOfficePath))
                    {

                        result = false;
                        throw new Exception("Operating System not supported for LibreOffice");
                    }
                    else
                    {
                        if (!File.Exists(libOfficePath))
                        {
                            result = false;
                            throw new Exception("Missing LibreOffice Installation");
                        }
                        ProcessStartInfo proc = new ProcessStartInfo(libOfficePath, cmdLine);
                        proc.RedirectStandardOutput = true;
                        proc.UseShellExecute = false;
                        proc.CreateNoWindow = true;
                        proc.WorkingDirectory = Globals.TempPath;
                        Process process = new Process() { StartInfo = proc };
                        process.Start();
                        process.WaitForExit(30000);
                        if (process.ExitCode != 0)
                        {
                            throw new Exception("LibreOffice Exit Code" + process.ExitCode.ToString());
                        }


                    }

                    result = PrintPdf(Path.Combine(Globals.TempPath, newPdfName + ".pdf"));
                }
                catch (InvalidOperationException ex)
                {
                    ErrLog += "There is an error in the LibreOffice Console Application which did not exit gracefully. CmdLine was: " + cmdLine;
                    return false;
                }
                catch (Exception ex)
                {
                    ErrLog += "Unable to Print document file " + fileToPrint + ". " + ex.Message;
                    return false;
                }


                try
                {
                    File.Delete(Path.Combine(Globals.TempPath, newPdfName + ".pdf"));
                }
                catch (Exception ex)
                {
                    ErrLog += "Unable to Delete Temporary file " + Path.Combine(Globals.TempPath, newPdfName + ".pdf") + " after Printing. ";
                }
                return result;
            }
            else
            {
                ErrLog += "Cannot Print document. File does not exist";
                return false;
            }
        }

        private string GetLibOfficePath()
        {
            switch (Environment.OSVersion.Platform)
            {
                case PlatformID.Unix:
                    return "/usr/bin/office/";
                case PlatformID.Win32NT:
                    return @"C:\Program Files\LibreOffice\program\soffice.exe";
                default:
                    throw new PlatformNotSupportedException("OS is not supported");
            }
        }

        public bool PrintPdf(string fileToPrint)
        {
            try
            {

                ProcessStartInfo info = new ProcessStartInfo(fileToPrint); //in this pass the file path
                if (!string.IsNullOrEmpty(_printQueue))
                {
                    info.Arguments = "\"" + _printQueue + "\"";
                }
                info.Verb = "print";

                info.CreateNoWindow = true;
                info.WindowStyle = ProcessWindowStyle.Hidden;
                var process = Process.Start(info);
                if (process == null)
                {

                }
                else
                {
                    process.WaitForExit(10000);
                }

                return true;

            }
            catch (Exception ex)
            {
                ErrLog += "Unable to print file. " + fileToPrint + ". " + ex.Message;
                return false;
            }
        }
        #endregion


        #region Helpers

        public void DoPrint()
        {
            PrintDocument pd = new PrintDocument();
            pd.PrintPage += new PrintPageEventHandler(pd_PrintPage);
            pd.PrinterSettings.PrinterName = _printQueue;

        }
        private void pd_PrintPage(object sender, PrintPageEventArgs ev)
        {
            float linesPerPage = 0;
            float yPos = 0;
            int count = 0;
            float leftMargin = ev.MarginBounds.Left;
            float topMargin = ev.MarginBounds.Top;
            String line = null;

            // Calculate the number of lines per page.
            linesPerPage = ev.MarginBounds.Height /
               printFont.GetHeight(ev.Graphics);

            // Iterate over the file, printing each line.
            while (count < linesPerPage &&
               ((line = streamToPrint.ReadLine()) != null))
            {
                yPos = topMargin + (count * printFont.GetHeight(ev.Graphics));
                ev.Graphics.DrawString(line, printFont, Brushes.Black,
                   leftMargin, yPos, new StringFormat());
                count++;
            }

            // If more lines exist, print another page.
            if (line != null)
                ev.HasMorePages = true;
            else
                ev.HasMorePages = false;
        }
        #endregion






    }
}
