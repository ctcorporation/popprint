﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace PopPrint_Service.Classes
{
    public class JobProcessor : IDisposable
    {
        private bool disposedValue;
        #region members

        #endregion

        #region properties

        #endregion

        #region constructors
        public JobProcessor()
        {

            DoWork();
        }


        #endregion

        #region methods

        private Mailboxes LoadProfiles(string profileXml)
        {
            Mailboxes mailboxProfiles = new Mailboxes();
            var m = MethodInfo.GetCurrentMethod();
            if (!File.Exists(profileXml))
            {
                PopPrintResources.WriteLog(string.Empty, PopPrintResources.EventCode.E203, "Profile XML Not Found", "Unable to continue. No Mailbox Profile Found. Creating blank", null, null);
                CreateProfileXML(Globals.ProfileXml);
            }

            try
            {
                using (FileStream fs = new FileStream(profileXml, FileMode.Open, FileAccess.Read))
                {
                    XmlSerializer xs = new XmlSerializer(typeof(Mailboxes));
                    mailboxProfiles = (Mailboxes)xs.Deserialize(fs);
                    fs.Close();
                }
            }
            catch (IOException ex)
            {
                var err = ex.GetType().Name + " " + ex.Message + Environment.NewLine + ex.InnerException;
                PopPrintResources.WriteLog(string.Empty, PopPrintResources.EventCode.E202, m.Name, err, null, null);
            }
            catch (InvalidOperationException ex)
            {
                var err = ex.GetType().Name + " " + ex.Message + Environment.NewLine + ex.InnerException;
                PopPrintResources.WriteLog(string.Empty, PopPrintResources.EventCode.E204, m.Name, err, null, null);
            }
            catch (Exception ex)
            {
                var err = ex.GetType().Name + " " + ex.Message + Environment.NewLine + ex.InnerException;
                PopPrintResources.WriteLog(string.Empty, PopPrintResources.EventCode.E204, m.Name, err, null, null);
                CreateProfileXML(Globals.ProfileXml);


            }
            return mailboxProfiles;
        }

        private void CreateProfileXML(string profileXml)
        {
            XDocument xmlProfileConf = new XDocument(
                new XDeclaration("1.0", "UTF-8", "Yes"),
                new XElement("Mailboxes",
                    new XElement("Mailbox",
                     new XElement("Rule",
                                        new XElement("Name"),
                                        new XElement("IsActive"),
                                        new XElement("PopAction"),
                                        new XElement("PopDelete"),
                                        new XElement("ArchiveFile"),
                                        new XElement("ArchiveLocation"),
                                        new XElement("AddressFrom"),
                                        new XElement("Subject"),
                                        new XElement("SaveLocation"),
                                        new XElement("SaveDomain"),
                                        new XElement("SaveUser"),
                                        new XElement("SavePassword"),
                                        new XElement("PrintQueue"),
                                        new XElement("FtpServer"),
                                        new XElement("FtpPort"),
                                        new XElement("FtpUserName"),
                                        new XElement("FtpPassword"),
                                        new XElement("FtpKeyPath"),
                                        new XElement("FtpFingerPrint"),
                                        new XElement("FtpSecure"),
                                        new XElement("FtpSSLCertifcate"),
                                        new XElement("FtpFolder")
                                                   ),
                        new XElement("PopServer"),
                        new XElement("PopUserName"),
                        new XElement("PopPassword"),
                        new XElement("PopPort"),
                        new XElement("PopSSL"),
                        new XElement("AddressTo")

                                     )));

            while (File.Exists(profileXml))
            {
                File.Delete(profileXml);
            }

            xmlProfileConf.Save(profileXml);

            LoadProfiles(profileXml);
        }
        private void DoWork()
        {
           // System.Diagnostics.Debugger.Launch();
            var m = MethodInfo.GetCurrentMethod();
            var profs = LoadProfiles(Globals.ProfileXml);
            // Load Mailbox List
            List<MailboxesMailbox> mailBoxList = profs.Items.ToList();
            if (mailBoxList != null)
            {
                foreach (MailboxesMailbox mailbox in mailBoxList)
                {
                    ProfileMailServer mserver = new ProfileMailServer();
                    mserver.Hostname = mailbox.PopServer;
                    mserver.Port = Convert.ToInt16(mailbox.PopPort);
                    mserver.UserName = mailbox.PopUserName;
                    mserver.Password = mailbox.PopPassword;
                    mserver.SSL = mailbox.PopSSL;
                    mserver.AddressTo = mailbox.AddressTo;
                    //Create list of Messages from Current mailbox.                    
                    try
                    {
                        var msgResult = MailModule.MessageCount(mserver.Hostname, mserver.Port, mserver.SSL, mserver.UserName, mserver.Password);
                        if (msgResult.Item2)
                        {
                            if (msgResult.Item1 > 0)
                            {
                                List<MsgSummary> msgList = MailModule.GetMail(mserver.Hostname, mserver.Port, mserver.SSL, mserver.UserName, mserver.Password);
                                foreach (MsgSummary msg in msgList)
                                {
                                    bool deleteMessage = ProcessMsg(msg, mailbox);
                                    if (msg.MsgStatus.Contains("Error"))
                                    {
                                        PopPrintResources.WriteLog(string.Empty, PopPrintResources.EventCode.E201, m.Name, "Message Skipped due to Error: " + msg.MsgStatus + " in Message: (" + msg.MsgSubject + ")", null, msg);
                                        deleteMessage = true;
                                    }
                                    if (deleteMessage)
                                    {
                                        MailModule.DeleteMessage(mserver.Hostname, mserver.Port, mserver.SSL, mserver.UserName, mserver.Password, msg.MsgId);
                                    }
                                }
                            }
                        }
                        else
                        {
                            PopPrintResources.WriteLog(string.Empty, PopPrintResources.EventCode.E101, m.Name, "Warning.Unable to Connect to" + mserver.Hostname + " for mailbox : " + mserver.AddressTo + ".", null, null);
                        }
                    }
                    catch (Exception ex)
                    {
                        PopPrintResources.WriteLog(string.Empty, PopPrintResources.EventCode.E101, m.Name, ex.GetType().Name + " Error found while checking " + mserver.Hostname + " for mailbox : " + mserver.AddressTo + " Error Message: " + ex.Message, null, null);

                    }
                }
            }
        }

        private bool ProcessMsg(MsgSummary msg, MailboxesMailbox mailbox)
        {
            var m = MethodInfo.GetCurrentMethod();
            bool messageProcessed = false;
            if (msg.MsgFile != null)
            {
                var mbRule = GetMbRule(mailbox, msg);
                if (mbRule == null)
                {
                    PopPrintResources.WriteLog(string.Empty, PopPrintResources.EventCode.E201, m.Name, "Message is not valid: " + msg.MsgStatus + " in Message: (" + msg.MsgSubject + ")", null, null);
                    messageProcessed = true;
                    // No rules found for this email. Skip it and continue with next email.                                             
                }
                if (mbRule.PopAction == "Print" && string.IsNullOrEmpty(mbRule.PrintQueue))
                {
                    PopPrintResources.WriteLog(string.Empty, PopPrintResources.EventCode.E405, m.Name, "No Print Queue specified", mbRule, msg);
                    messageProcessed = false;

                }
                if (!File.Exists(Globals.HtmlPath))
                {
                    PopPrintResources.WriteLog(string.Empty, PopPrintResources.EventCode.E205, m.Name, "System Default HTML Template is missing and cannot print", null, msg);
                    return false;
                }
                HtmlMsg htmlMsg = new HtmlMsg(msg);

                if (string.IsNullOrEmpty(htmlMsg.PdfPath))
                {

                }
                else
                {
                    if (mbRule.PopAction == "Print")
                    {
                        var filePrinted = PrintDoc(htmlMsg.PdfPath, mbRule.PrintQueue);
                        if (filePrinted.Item1)
                        {
                            PopPrintResources.WriteLog(htmlMsg.PdfPath, PopPrintResources.EventCode.I400, m.Name, "File Printed.", mbRule, msg);
                            File.Delete(htmlMsg.PdfPath);
                        }
                        else
                        {
                            PopPrintResources.WriteLog(htmlMsg.PdfPath, PopPrintResources.EventCode.E404, m.Name, filePrinted.Item2, mbRule, msg);
                        }
                        if (mbRule.PopDelete)
                        {
                            messageProcessed = true;
                        }
                    }
                }
                //Remove the image01.jpg/png ie the signature images
                CleanLogosFromAttachments(msg.MsgFile);
                foreach (MsgAtt att in msg.MsgFile)
                {
                    switch (mbRule.PopAction)
                    {
                        case "FTP":
                            var res = SendFile(mbRule, att);
                            switch (res)
                            {
                                case "WINSCP":
                                    PopPrintResources.WriteLog(att.AttFilename.FullName, PopPrintResources.EventCode.E309, m.Name, "WINSCP.EXE problem.", mbRule, msg);
                                    break;
                                case "SUCCESS":
                                    PopPrintResources.WriteLog(att.AttFilename.FullName, PopPrintResources.EventCode.I300, m.Name, "File Transferred.", mbRule, msg);
                                    break;
                                case "FAILED":
                                    PopPrintResources.WriteLog(att.AttFilename.FullName, PopPrintResources.EventCode.E302, m.Name, "File Failed to Transfer.", mbRule, msg);
                                    break;
                                case "CONNECTION":
                                    PopPrintResources.WriteLog(att.AttFilename.FullName, PopPrintResources.EventCode.E308, m.Name, "FTP Connection eror.", mbRule, msg);
                                    break;
                            }
                            break;
                        case "Print":

                            string fileType = att.AttFilename.Extension;
                            var filePrinted = PrintDoc(att.AttFilename.FullName, mbRule.PrintQueue);
                            if (filePrinted.Item1)
                            {
                                PopPrintResources.WriteLog(att.AttFilename.FullName, PopPrintResources.EventCode.I400, m.Name, "File Printed Ok.", mbRule, msg);
                            }
                            else
                            {
                                PopPrintResources.WriteLog(att.AttFilename.FullName, PopPrintResources.EventCode.E404, m.Name, filePrinted.Item2, mbRule, msg);
                                messageProcessed = false;
                            }
                            break;
                        case "Save":
                            if (Directory.Exists(mbRule.SaveLocation))
                            {
                                try
                                {
                                    att.AttFilename.MoveTo(Path.Combine(mbRule.SaveLocation, att.AttFilename.Name));
                                    messageProcessed = true;
                                }
                                catch (IOException ex)
                                {
                                    var exMsg = ex.GetType().Name + " error saving file. Error was " + ex.Message;
                                    messageProcessed = false;
                                }
                            }
                            else
                            {

                            }
                            break;

                    }
                }
            }
            return messageProcessed;
        }

        private void CleanLogosFromAttachments(List<MsgAtt> msFiles)
        {
            foreach (MsgAtt att in msFiles)
            {
                if (att.AttFilename.Extension.Contains("jpg") || att.AttFilename.Extension.Contains("png"))
                {
                    if (att.AttFilename.Name.Contains("image"))
                    {
                        if (File.Exists(att.AttFilename.FullName))
                        {
                            File.Delete(att.AttFilename.FullName);
                        }
                        // Other Image file exists. 

                    }
                }
            }

        }

        public Tuple<bool, string> PrintDoc(string filetoPrint, string varPrintTo)
        {
            var m = MethodInfo.GetCurrentMethod();
            string err = string.Empty;
            bool result = false;
            FileInfo fi = new FileInfo(filetoPrint);
            var printDoc = new Print(varPrintTo);
            string[] docExts = { ".doc", ".docx", ".xls", ".xlsx", ".txt", ".csv" };
            string[] imgExts = { ".jpg", ".png", ".bmp", ".jpeg" };

            if (fi.Extension.ToLower() == ".pdf")
            {
                result = printDoc.PrintPdf(filetoPrint);
            }
            else
            {
                if (docExts.Contains(fi.Extension.ToLower()))
                {
                    result = printDoc.PrintDoc(filetoPrint);
                }
                else
                {
                    if (imgExts.Contains(fi.Extension.ToLower()))
                    {
                        if (File.Exists(filetoPrint))
                        {
                            result = printDoc.PrintImagePage(filetoPrint);
                        }
                    }
                    else
                    {
                        return new Tuple<bool, string>(false, "File type " + Path.GetExtension(filetoPrint) + " not currently supported in printing function");
                    }
                }
            }


            if (!result)
            {
                err = printDoc.ErrLog;
            }

            try
            {
                if (File.Exists(filetoPrint))
                {
                    File.Delete(filetoPrint);
                }
            }
            catch (IOException ex)
            {
                err = ex.GetType().Name + " " + ex.Message;
            }

            return new Tuple<bool, string>(result, err);

        }

        #endregion

        #region helpers

        private MailboxesMailboxRule GetMbRule(MailboxesMailbox mailbox, MsgSummary msg)
        {
            var ruleList = mailbox.Rule.ToList();
            var rule = (from x in ruleList
                        where (x.AddressFrom.ToLower() == msg.MsgFrom.ToLower() || x.AddressFrom == "*")
                      && (x.Subject == msg.MsgSubject || x.Subject == "*" || x.IsActive == true)
                        select x).FirstOrDefault();
            return rule;
        }

        private string SendFile(MailboxesMailboxRule mbRule, MsgAtt att)
        {
            var ftpHelper = new FTPHelper(mbRule, att);
            return ftpHelper.SendFile();

        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects)
                }

                // TODO: free unmanaged resources (unmanaged objects) and override finalizer
                // TODO: set large fields to null
                disposedValue = true;
            }
        }

        // // TODO: override finalizer only if 'Dispose(bool disposing)' has code to free unmanaged resources
        // ~JobProcessor()
        // {
        //     // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
        //     Dispose(disposing: false);
        // }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
        #endregion

    }
}
