﻿using PopPrint_Service.Classes;
using System;
using System.IO;
using System.ServiceProcess;

namespace PopPrint_Service
{
    public partial class PopPrint : ServiceBase
    {

        public System.Timers.Timer tmrMain;

        public PopPrint()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {

            LoadSettings();
            PopPrintResources.WriteLog(string.Empty, PopPrintResources.EventCode.I101, "Status Change", "Service Started.", null, null);
            tmrMain = new System.Timers.Timer();
            tmrMain.Interval = (int)TimeSpan.FromMinutes(Globals.TimerInt).TotalMilliseconds;
            tmrMain.Elapsed += new System.Timers.ElapsedEventHandler(tmrMain_Elapsed);
            tmrMain.Start();

        }

        private void LoadSettings()
        {
            string s = Path.Combine(Globals.SettingsPath, Globals.AppName + ".xml");
            SystemSettings systemSettings = new SystemSettings(s);
            var profPath = systemSettings.GetElement("SystemSettings", "ProfileLocation");
            if (profPath != null)
            {
                Globals.AppProfilePath = profPath.Value;
                Globals.ProfileXml = Path.Combine(Globals.AppProfilePath, "PopPrintProfiles.xml");
            }
            var logpath = systemSettings.GetElement("SystemSettings", "LogLocation");
            if (logpath != null)
                Globals.LogPath = logpath.Value;
            var winscp = systemSettings.GetElement("SystemSettings", "WinSCPLocation");
            if (winscp != null)
                Globals.WinSCPPath = winscp.Value;
            var htmlTemplate = systemSettings.GetElement("SystemSettings", "HtmlTemplate");
            if (htmlTemplate != null)
                Globals.HtmlPath = htmlTemplate.Value;
            var runOnstart = systemSettings.GetElement("SystemSettings", "RunOnStart");
            if (runOnstart != null)
                Globals.RunOnStart = bool.Parse(runOnstart.Value);
            var attLoc = systemSettings.GetElement("SystemSettings", "AttachmentLocation");
            if (attLoc != null)
                Globals.AttachmentPath = attLoc.Value;
            var timer = systemSettings.GetElement("SystemSettings", "TimerInterval");
            if (timer != null)
                Globals.TimerInt = int.Parse(timer.Value);
            var arcFreq = systemSettings.GetElement("ArchiveSettings", "ArchiveFrequency");
            if (arcFreq != null)
                Globals.ArcFrequency = arcFreq.Value;
        }

        protected override void OnStop()
        {
            PopPrintResources.WriteLog(string.Empty, PopPrintResources.EventCode.I101, "Status Change", "Service has been Stopped.", null, null);
            tmrMain.Stop();
        }


        private void tmrMain_Elapsed(object sender, EventArgs e)
        {
            ProcessJobs();
        }

        private void ProcessJobs()
        {
            using (JobProcessor jobProcessor = new JobProcessor())
            {

            }

        }
    }
}
