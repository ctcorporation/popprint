﻿namespace PopPrint_Service
{
    partial class PopPrintInstaller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ppServiceProcessInstaller = new System.ServiceProcess.ServiceProcessInstaller();
            this.ppServiceInstaller = new System.ServiceProcess.ServiceInstaller();
            // 
            // serviceProcessInstaller1
            // 
            this.ppServiceProcessInstaller.Password = null;
            this.ppServiceProcessInstaller.Username = null;

            // 
            // serviceInstaller1
            // 
            this.ppServiceInstaller.ServiceName = "PopPrint";
            this.ppServiceInstaller.Description = "PoPPrint from CTC is an email automation program that allows email to Print, email to FTP and email to Folder services";
            this.ppServiceInstaller.DisplayName = "PopPrint Service";
            // 
            // ProjectInstaller
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.ppServiceProcessInstaller,
            this.ppServiceInstaller});

        }

        #endregion

        private System.ServiceProcess.ServiceProcessInstaller ppServiceProcessInstaller;
        private System.ServiceProcess.ServiceInstaller ppServiceInstaller;
    }
}