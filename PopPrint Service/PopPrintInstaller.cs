﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Linq;
using System.Threading.Tasks;

namespace PopPrint_Service
{
    [RunInstaller(true)]
    public partial class PopPrintInstaller : System.Configuration.Install.Installer
    {
        public PopPrintInstaller()
        {
            InitializeComponent();
        }
    }
}
