﻿namespace PopPrint_Service
{
    public class Globals
    {

        public const string SettingsPath = @"C:\Apps\PopPrint";
        public const string AppName = "PopPrint";
        public const string LogFileName = "PopPrintLog.XML";
        public static string AppProfilePath { get; set; }


        public static string ProfileXml { get; set; }

        public static string ArcFrequency { get; set; }

        public static string LogPath { get; set; }

        public static int TimerInt { get; set; }

        public static string TempPath { get; set; }

        public static string AttachmentPath { get; set; }

        public static bool RunOnStart { get; set; }
        public static string WinSCPPath { get; internal set; }
        public static string HtmlPath { get; set; }
    }
}
